# backup

A python program for file backup

Purpose: A python graphical user interface ( GUI ) program for file backup.  For easy access to the backups b
Backups are in the same file format and directories as the source files.
Backups overwrite old versions, but there are no deletes executed by the backup. Also:
* Extensive logging of the backup operations.  *
* Provides for a smulated backup.
* Recurses down a directory tree.
* May easily be restricted to specific file types
* And so on... 

Enviroment: 
* Program may run on any OS supporting Python 3.6., except for some details where I may have used Windows only functions. 
* So far only tested on:
** Windows 
 
Program Status: Beta, works for me, but ugly GUI and not fully tested.  Use if you do not mind a few glitches.

Much more documentation at:  https://www.2020.opencircuits.com/index.php?title=Python_Backup_Program ( hopefully, wiki is having some problems, message me if there are problems )

``` 
	Standard Disclaimer:
		If you have more than a casual interest in this project you should contact me 
		( no_spam_please_666 at comcast.net ) and see if the repository is actually in good shape.  
		I may well have improved software and or documentation.  
		I will try to answer all questions and perhaps even clean up what already exists.	
``` 		

Guide to Repository

* Root  - minor documentation, mostly README
* src   -  Source and runtime:  required to run the application.  I think it qualifies as pure python ( 3.6 ).  See introduction here for the files containing the "main programs".
* src/TestSource   - directory/files to backup as a test
* src/TestDest     - directory for target of backup, must create /TestDest/TestSource ( usually empty ) to enable backup
* images - various graphics: screen shots, and extra icons....
* wiki_etc - including pdf versions of the wiki at open circuits.
* a wiki here, very little.
