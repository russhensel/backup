# -*- coding: utf-8 -*-



"""
this file is .........\_sample_backup_3.py or was copied from it

this version requires a custom function in parameters, but then you can
just put a copy in a directory and run it to backup that directory.
it uses the current directory it is found in as the source and
the subroutine determines the destination directory

note how little code it actually contains

use it as a template for your own backup files
as shipped it is close ( some adjustment needed ) to test backing up sample subdirectories
in this application

you need a path fix for this to work ??  at least on windows on a double click
the system needs to find python.exe.  perhaps a file association will do it
hard for me to tell because it works for me.


"""

import  sys
import  directory_backup


"""
this file is not the application, it is tucked away here
"""
app = directory_backup.App(  )


"""
no name set, default from parameters -- will be used for many backups
"""


"""
both the source and the destination take the same arguments:
    * the source directory
    * the media id -- not explained here ( but is in the help file at
             Backup Help File - OpenCircuits
             http://www.opencircuits.com/Backup_Help_File )
      and has no function if set to "ignore"

"""

app.parameters.set_source_here( "data"  )   # see parameters.py


"""
set the location of the detail and summary log -- here defaulted from parameters.py so nothing here

"""


"""
if set_simulate_mode is set to True, no actual copies are done but you can see what would have been done in the logs
"""


"""
skip the filter it defaults to all


"""


"""
run it
"""
app.run_gui()

"""
clean up
"""
app.clean_up()   # cant this be moved inside


#======================= Notes: ========================

"""
You might want to keep some notes here


"""