# -*- coding: utf-8 -*-



"""
this file is .........\_sample_backup_2.py or was coppied from it

this version requires a bit more setup in parameters, but then some of the
settings default and you can pretty much forget about them
you can make copies of this file and pretty much just change the
source and destination and you are done

use it as a template for your own backup files
as shipped it is close ( some adjustment needed ) to test backing up sample subdirectories
in this application

do we need a path fix for this to work ??


"""

import  sys
import  directory_backup


"""
this file is not the application, it is tucked away here
"""
app = directory_backup.App(  )


"""
no name set, default from parameters -- will be used for many backups
"""


"""
both the source and the destination take the same arguments:
    * the source directory
    * the media id -- not explained here ( but is in the help file at
             Backup Help File - OpenCircuits
             http://www.opencircuits.com/Backup_Help_File )
      and has no function if set to "ignore"

"""
app.set_source( r"D:\Russ\0000\python00\python3\_projects\backup\backup_test\TestSource",             "ignore" )

app.set_dest(   r"D:\Russ\0000\python00\python3\_projects\backup\backup_test\TestDest\TestSource",     "ignore" )


"""
set the location of the detail and summary log -- here defaulted from parameters.py so nothing here

"""



"""
if set_simulate_mode is set to True, no actual copies are done but you can see what would have been done in the logs
"""


"""
skip the filter it defaults to all


"""


"""
run it
"""
app.run_gui()

"""
clean up
"""
app.clean_up()   # cant this be moved inside


#======================= Notes: ========================

"""
You might want to keep some notes here


"""