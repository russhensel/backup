# -*- coding: utf-8 -*-

"""
parameters for directory backup

"""

import logging
import os
import sys


# ----------- local -------------
from   app_global import AppGlobal
import directory_backup

class Parameters( object ):
    """
    for directory backup

    note:
    some parameters may be marked as *todo !!
    may want to customize this file to the computer on which the backup is run like my other parameter files

    """
    def __init__(self, ):


          # ================== required stuff, do not change   ================
          AppGlobal.parameters  = self                    # ** do not change - save a link parameters in AppGlobal
          self.controller       = AppGlobal.controller    # ** do not change - save a link to the controller not clear needed here
          self.our_os = sys.platform       #testing if our_os == "linux" or our_os == "linux2"  "darwin"  "win32"
          if self.our_os == "win32":
              self.os_win = True     # the OS is windows any version
          else:
              self.os_win = False    # the OS is not windows

          # ================== the parameters that typical uses may find useful  ================

          #  ------ perhaps get this ?? from introspection of this class -- experiment with stuff
          self.application_dir    = r"D:\Russ\0000\python00\python3\_projects\backup"  # set this to location of the app = this file

          self.application_dir    = os.path.dirname(__file__)  # one of many introspection approaches


          # ----- backup control
          # self.max_dir_depth     = -1     # -1 unlimited 0 = no recursion down dir, else depth below initial
          self.max_dir_depth       = -1   # -1 for unlimited ( untill we crash ), 0 = no recursion down dir

          self.simulate_mode_flag  = False   # set to true to just simulate, no copy, this is a default, you may override later

          self.default_name        = "Un-named Backup"

          # ------ logging
          self.pylogging_fn        = "directory_backup.pylog"               # python logging, we also have other logging
          #self.pylogging_fn      = r"c:/logs/directory_backup.pylog"      # python logging, we also have other logging
          self.logging_level       = logging.DEBUG          #   CRITICAL	50   ERROR	40 WARNING	30  INFO	20 DEBUG	10 NOTSET	0
          self.logging_level       = logging.INFO


          self.log_detail_fn       = r"D:\logs_rsh\BackuDetail2019.log"
          self.log_summary_fn      = r"D:\logs_rsh\BackSummary2019.log"

          self.log_skipped_flag    = True     # True or false
          self.mode                = "default"

          # ----- misc
          """
          enables editing viewing of files from the gui
          """
          self.ex_editor            =  r"c:\apps\Notepad++\notepad++.exe"    # russ win 10;  d: on smithers c on millhouse

          """
          size and position of the gui
          """
          self.root_geometry       = "1000x900+40+80"   # control window size and position
          #--------------- appearance ---------
          self.win_geometry      = self.root_geometry      # window width x height + position x + position y
          self.id_color          = "red"    # the application may have color to help identify it.  "blue"   "green"  and lots of other work
          self.id_height         = 20       # if there is an id pane, height of id pane, 0 for no pane

          # tkinter uses bg for background so I should probably make a global change
          self.bkg_color         = "blue"   # color for the background, you can match the id color or use a neutral color like gray
          self.bkg_color         = "gray"   # override of above because I could
          self.bkg_color         = 'dark slate gray'
          self.btn_color         = "gray"   # color for buttons -- may not be implemented
          #self.btn_color         = "gray"   # color for buttons -- may not be implemented
          self.help_file         =  r"http://www.opencircuits.com/Backup_Help_File"

          """
          so far I only have icons for windows -- change if you can find valid files
          """
          if  self.os_win:
            pass
            #self.icon              = r"./green_house.ico"    #  default gui icon -- greenhouse this has issues on rasPi - need new icon for smartplug
            #self.icon              = None
            self.icon               = "./temp_test.ico"    # because we run from another directory this does not work
            self.icon               = r"D:\Russ\0000\python00\python3\_projects\backup\disk_drive.ico"
          else:
            pass
            self.icon              = None

        # ================== the parameters for internal operation, generally you do not change ================
        # ----- message area in gui   -----------
          self.default_scroll      = 1        # 1 auto scroll the message area, else 0
          self.max_lines           = 1000
          self.max_detail_lines    = self.max_lines    # max lines on the detail size ( may include skipped even if off for log file )

          self.queue_sleep         = 2          # time for backup to sleep if queue fills
          self.tk_queue_sleep      = 2          # time in seconds  -- this is new name aqueue_sleep is on its way out

          self.poll_delta_t        = 50         # in ms --   lowest I have tried is 10 ms, could not see cpu load
          self.queue_length        = 50
          self.ix_queue_max        = 10         # number of times queue sleep is allowed to loop before error

          self.pause_sleep         = 1          # time for sleep in pause

# ------------------------------------------------------------
    def init_2(self, ):
        """
        second init that needs to be done late just before gui start
        this defaults values still set to None ( why not just initialize earlier -- because
        objects not created .... still seems odd but could have in parameter and init from there
        and over write )
        """
        print( "init_2", flush = True )
        app_state       = AppGlobal.app_state

        if len( app_state.filter_objects ) == 0:        # it defaults to  []
            filter_object  = directory_backup.FFAll( )
            AppGlobal.controller.add_filter_object( filter_object )

# ------------------------------------------------------------
    """
    used by caller prior to running backup
    move to parameters so all differences are there
    """
    # ------------------------------------------
    def set_source_here( self, to_where ):
        """
        backup from directory with backup file "here" to a location determined
        by to_where, where this needs to be customized for each value of the argument
        the to_where location is just a mnemonic for the user to select
        code here form what amounts to a switch statement ( if elif elif.... )
        """
        app_state                      = AppGlobal.app_state
        app_state.backup_name          = f"Backup from here to {to_where}"
        app_state.sourceDir            = app_state.initial_dir
        app_state.sourceMediaId        = "ignore"
        if to_where == "source":
            dest_dir    = os.path.splitdrive( here )[1]
            dest_dir    = dest_dir[1:]
            dest_dir    = os.path.join( "I:\_Source", dest_dir  )
            self.set_dest( dest_dir, "Buffalo 3T Disk" )
        if to_where == "data":
            dest_dir    = os.path.splitdrive( app_state.initial_dir )[1]
            dest_dir    = dest_dir[1:]
            dest_dir    = os.path.join( "I:\_Data", dest_dir  )
            self.controller.set_dest( dest_dir, "Buffalo 3T Disk" )
        elif to_where == "what the f":
            """
            you might want to add more here
            """
            pass
        else:
            # !! may want to add to error count so backup cannot be run - likely some error will be set off any way
            print( f"invalid to_where {to_where} ")

    # ------------------------------------------
    def check_parms( self  ):
        """
        variables defined
        file name of editors.... exist
        """
        pass

#        if os.path.isfile( db_file_name ):



if __name__ == "__main__":
    print("")
    print("================= do not run me please, I have nothing to say  ===================")


# =================== eof ==============================


