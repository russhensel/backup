# -*- coding: utf-8 -*-
#
#    !! limit on max depth may not be working
#
#    !! shut this down with an exception -- will this work, catch back in
#    !! the thread
#

import stat
import time
import shutil
import os
import os.path
#import queue
import logging
import datetime
import sys
import traceback
from pathlib import Path
import pathlib

# local
import gui
from   app_global import AppGlobal


class BackupThread( object ):
    """
    this is the worker helper thread, as distinct from the gui thread
    """
    def __init__( self,  ):
        self.controller      = AppGlobal.controller
        self.parameters      = AppGlobal.parameters
        self.app_state       = AppGlobal.app_state
        print(  f"{AppGlobal.logger_id}.{ self.__class__.__name__ }" )
        self.logger          = logging.getLogger( f"{AppGlobal.logger_id}.{ self.__class__.__name__ }" )

        self.logger.info( "DirectoryBackup backup_thread init")        # info debug...
        self.logSummaryFile  = None
        self.logDetailFile   = None
        self.run_function    = self.copy_tree   # may be changed by set functions in this guy or in directory_backup
        print( f"logger level in BackupThread = {self.logger.level}" )

        #self.reset_parms()
        #self.reset_progress()

    # ------------------------------------------------
    def run_backup( self ):
        """
        call from main thread, this is what starts the thread
        not a direct call, but thru threading so start of new thread

        this is the startup  method for the thread
        run the whole backup
        currently start from beginning and run to end unless stopped
        !! not sure how we set up the parms, for now in the init
        !! not all need to be in a thread
        this is where logging exists,
        assume that we have checked that it is ok to start
        """

        # need to be sure that parms are checked and is ok to go

        self.open_logs()
        self.logBothLine( "" )
        self.logBothLine( "==========================================="  )
        self.logBothLine( "Russ's python backup ( backup_thread using copy_tree_new ) " )
        self.logBothLine( "Beginning backup from"  )
        self.logBothLine( self.app_state.sourceDir  )
        self.logBothLine( "     to"   )
        self.logBothLine( self.app_state.destDir )
        self.logBothLine( ""   )
        self.logBothLine( self.controller.app_name  + " " + self.controller.app_version   )
        self.logBothLine( "Directory depth " + str( self.app_state.max_dir_depth  ) )
        self.logBothLine( "Filter test " +   self.controller.get_filter_text()   )

        self.app_state.start_ts   =  time.time()
        dt_obj                    = datetime.datetime.utcfromtimestamp( self.app_state.start_ts )
        string_rep                = dt_obj.strftime('%Y-%m-%d %H:%M:%S')
        self.logBothLine( "Time now: " + string_rep )
		
        try:
            #self.copy_tree( self.app_state.sourceDir, self.app_state.destDir )
#            if self.parameters.use_new:
#                self.logger.critical( "parameters say use_new()" )
#                self.copy_tree_new( self.app_state.sourceDir, self.app_state.destDir )
#            else:

#            self.copy_tree(    self.app_state.sourceDir, self.app_state.destDir )
            self.run_function( self.app_state.sourceDir, self.app_state.destDir )

        except  Exception as exception:
            # Catch the custom exception
            # may be having unicode problems??
            #ex_as_string     = str( encode( ))
            print( f"exception in run_backup() >>{exception}<<" )
            s_trace = traceback.format_exc()
            print( f"format-exc>>{s_trace}<<" )

            self.logger.critical( f"exception in run_backup() >>{exception}<<" , stack_info=True )
            #self.logger.critical( str( exception ), stack_info=True )  # unicode error here ??
            # reraise for testing !!
            raise

        self.app_state.set_state( "done" )
        self.app_state.end_ts   = time.time()
        dt_obj                  = datetime.datetime.utcfromtimestamp( self.app_state.end_ts )
        string_rep              = dt_obj.strftime('%Y-%m-%d %H:%M:%S')
        msg                     = "run_backup() done -- Time now: " + string_rep
        self.logBothLine( msg )
        self.close_backup()
        self.app_state.set_state( "done" )

    # ---------------------------------------
    def close_backup( self ):
        """
        call from worker thread only
        """
        self.logDetailLine(   "" )
        self.logBothLine(     "" )
        self.logBothLine(   "End of backup from"  )
        self.logDetailLine(   self.app_state.sourceDir  )
        self.logDetailLine(   "     to"   )
        self.logDetailLine(   self.app_state.destDir )
        self.logDetailLine(   ""   )
        #add mode and log skipped
        self.logBothLine(   "files added   = " + str( self.app_state.count_added       ) )
        self.logBothLine(   "files updated = " + str( self.app_state.count_updated     ) )
        self.logBothLine(  f"files skipped = {self.app_state.count_skipped}"  )

        self.logBothLine(  f"dir count     = {self.app_state.count_dir}"  )
        self.logBothLine(  f"dir added     = {self.app_state.count_dir_added}"  )
        self.logBothLine(  f"dir updated   = {self.app_state.count_dir_updated}"  )
        self.logBothLine(  f"dir skipped   = {self.app_state.count_dir_skipped}"  )


        self.logBothLine(  f"bytes copied  = {self.app_state.bytes_copied}"  )
        self.logBothLine(  f"bytes skipped = {self.app_state.bytes_skipped}"  )

        self.logBothLine(   "errors  = " + str( self.app_state.count_errors  ) )

        msg = gui.mega_etc( self.app_state.bytes_copied )
        self.logBothLine(   "bytes   = " + msg  )
        self.logBothLine(   ""  )
        self.logBothLine(   "========== End of backup ==========="  )
        self.logBothLine(   ""  )
        self.close_logs(  )

#    # ---------------------------------------
#    def logCount( self, count ):
#        """
#        thread only
#        """
#        print("log count " + str(count))  # keep
#        self.app_state.post_to_tk_queue( "count", count )


    # --------------------------------------------------
    def copy_tree( self, src, dst, symlinks=0 ):   # new but defective
        """
        call from backup thread only
        run in its own thread
        so what is new, what is defect
        this is really the backup thread called from run backup
        ** move a function into backup_thread object DirectoryBackup  was Pycopytree
        !! this logic is getting a little complicated
        src     is the string name of source directory
        dst     is the string name of destination directory
        """
        names = os.listdir( src )
        #listdir(      path)
        #Return a list containing the names of the entries in the directory.
        #The list is in arbitrary order. It does not include the special entries '.' and '..'
        #even if they are present in the directory. Availability: Macintosh, Unix, Windows.

        # make new directories in destination if needed ** -- that is not exist -- we know it is a directory
        # should be later ? normally dst is a directory

        self.app_state.count_dir       += 1

        if not os.path.isdir(dst):
            self.app_state.count_dir_added      += 1
            if not( self.app_state.simulate_mode_flag ):
                os.mkdir( dst )    #want to check for success??? *todo
        else:
            self.app_state.count_dir_updated      += 1

        self.app_state.post_to_tk_queue( ">display_dirs", "__msg" )    # is this were function should live ??

        for i_name in names:
            # file from / file to
            srcname = os.path.join( src, i_name )
            dstname = os.path.join( dst, i_name )

            while self.app_state.pause_flag:
                time.sleep( self.parameters.pause_sleep )

            if self.process_if_dir( srcname, dstname ):
                continue     # done with this name

            if self.process_filter( src, dst, srcname, dstname ):  #  name, src, dst, srcname, dstname
                continue

            self.process_copy(  srcname, dstname )

    # ---------------------------------------
    def process_if_dir( self, srcname, dstname ):
        """
        part of copy tree new --
        if dir test if can go deeper, if so create and recurse
        return True if processing complete - doen best we can on any error
               False - not a directory
        """
        # unicode issues in file names ??
        srcname_ascii    = srcname.encode( "ascii", "backslashreplace" ).decode(  )
        dstname_ascii    = dstname.encode( "ascii", "backslashreplace" ).decode(  )
        try:
            if os.path.isdir( srcname ): # if the reference is a directory then recursively function calls itself
                if ( ( self.app_state.max_dir_depth  == -1 )  or
                     ( self.app_state.ix_dir_depth   <   self.app_state.max_dir_depth ) ):

                    self.app_state.ix_dir_depth   += 1

                    #self.app_state.count_dir      += 1   # do not count, gets counted in copy_tree
                    self.app_state.post_to_tk_queue( ">display_dirs", "__msg" )

                    self.logger.info( f"dropping down a directory to {self.app_state.ix_dir_depth} for source {srcname_ascii}" )

                    self.copy_tree(srcname, dstname)

                    self.app_state.ix_dir_depth   += -1
                    self.logger.info( f"jumping up a directory to {self.app_state.ix_dir_depth}" )

#                   what was I thinking
#                    if ( self.app_state.max_dir_depth  != - 1 )  :
#                        self.app_state.ix_dir_depth   -= 1
                else:
                    self.logger.critical( "directory recursion limit reached - skipping " + srcname )
                    self.app_state.count_dir_skipped += 1
                done_flag  = True
            else:
                done_flag  = False   # not done because not a directory

        except (IOError, os.error) as why:
                msg = f"In process_if_dir(): Can't complete {srcname_ascii} to {destname_ascii}: exception {why}"
                self.logDetailLine( msg )
                # debug !!
                msg  = "error: " + srcname_ascii + " " +  dstname_ascii
                self.logDetailLine( msg )

                self.app_state.increment_error_cnt( msg )
                self.logger.critical( msg )
                self.logger.critical( str( why ), stack_info=True )
                done_flag  = True   # apparently we tried and failed, move on this is the best we can do

        return( done_flag )

    # ---------------------------------------
    def process_filter( self, src, dst, srcname, dstname ):
        """
        return True if processing complete do not copy the file, it is filtered out
        for now we are
        return
        """
        done_flag        = False    # False for file not filtered out -- filter itself True is for a filter in
        filter_objects   = self.app_state.filter_objects

        for i_filter_object in filter_objects:
             if not ( i_filter_object.check_file( srcname, src, dst  ) ):   # invert logic
                # skip_copy   = True
                msg  = f"filter object says skip:  {srcname}"

                if self.app_state.log_skipped_flag:  # 0 do not log skipped
                    self.logDetailLine( msg )
#                else:
#                    print(msg)   
                self.app_state.count_skipped += 1  # ?? move to increment method
                done_flag  = True
                break

        return done_flag

    # ---------------------------------------
    def process_copy( self, srcname, dstname ):
        """
        we know it is not a directory; it is a file, we will copy it
        that is copy, update, or skip

        !! looks like done_flag messed up but may not be used

        Return True if processing complete ( file copied or updated or skipped based on date  )
        """
        self.app_state.count_total    += 1   # each name is counted but srcname may be a directory
        done_flag                      = True   # assume true = copied file ( with or without error ? )

        srcname_ascii    = srcname.encode( "ascii", "backslashreplace" ).decode(  )
        dstname_ascii    = dstname.encode( "ascii", "backslashreplace" ).decode(  )

        srcStats = os.stat( srcname )
        #  see if new directory
        if not os.path.isfile( dstname ):
            # ok we now know the file dosn't exist in destination directory so must copy -- no date check
            msg  = "adding " + srcname_ascii
            self.logDetailLine( msg )

            self.app_state.count_added    += 1

            self.app_state.bytes_copied   += srcStats[6]     # count even if copy fails

            if not( self.app_state.simulate_mode_flag ):
                try:
                    shutil.copy2(srcname, dstname)
                except ( IOError, os.error ) as why:
                    msg = f"Can't copy process_copy io error {srcname_ascii} to {dstname_ascii}: exception {why}"
                    self.logDetailLine( msg )
                    # debug !!
                    msg  = "copy error: " + srcname_ascii + " -> " +  dstname_ascii

                    self.logDetailLine( msg )
                    self.app_state.increment_error_cnt( msg )
                    self.logger.critical( msg )
                    self.logger.critical( str( why ), stack_info=True )

        else:  # perhaps need update
            try:
                done_flag         = True
                srcStats          = os.stat( srcname )   # maybe placed lower
                mod_date_source   = ModifiedDate( srcname )
                mod_date_dst      = ModifiedDate( dstname ) + 1  # +1 for lower resolution in unix servers

                if mod_date_source > mod_date_dst:

                     msg  = "update " + dstname_ascii
                     self.logDetailLine( msg )

                     self.app_state.count_updated    += 1
                     self.app_state.bytes_copied     += srcStats[6]      # count even if copy fails

                     msg  = "dates "  + str( mod_date_source ) + "  " + str( mod_date_source )
                     self.logDetailLine( msg )

                     self.app_state.bytes_copied    += srcStats[6]
                     if not( self.app_state.simulate_mode_flag ):
                         self.copyFile( srcname, dstname )
                         #shutil.copy2( srcname, dstname )
                else:
                     msg  = f"skip based on date { srcname_ascii } dates { str(mod_date_source ) }  >>  {str( mod_date_dst ) }"

                     if self.app_state.log_skipped_flag:  # 0 do not log skipped
                         self.logDetailLine( msg )
#                     else:
#                         print(msg)  

                     self.app_state.count_skipped    += 1
                     self.app_state.bytes_skipped    += srcStats[6]     # do not count bytes of skipped unl


            except ( IOError, os.error ) as why:

                    msg = f"Can't update process_copy io error {srcname_ascii} to {dstname_ascii}: exception {why}"
                    self.logDetailLine( msg )
                    # debug !!
                    msg  = f"error: {srcname_ascii} -> {dstname_ascii}"

                    self.logDetailLine( msg )
                    self.app_state.increment_error_cnt( msg )
                    self.logger.critical( msg )
                    self.logger.critical( str( why ), stack_info=True )
            self.app_state.post_to_tk_queue( ">display_bytes", "__msg" )
            self.app_state.post_to_tk_queue( ">display_files", "__msg" )    # is this were function should live ??
        return done_flag


    # --------------------------------------------------
    def explore_tree( self, src, dst, symlinks=0 ):   # new but defective
        """
        call from backup thread only
        run in its own thread
        so what is new, what is defect
        this is really the backup thread called from run backup
        ** move a function into backup_thread object DirectoryBackup  was Pycopytree
        !! this logic is getting a little complicated
        src     is the string name of source directory
        dst     is the string name of destination directory
        """
        names = os.listdir( src )
        #listdir(      path)
        #Return a list containing the names of the entries in the directory.
        #The list is in arbitrary order. It does not include the special entries '.' and '..'
        #even if they are present in the directory. Availability: Macintosh, Unix, Windows.

        # make new directories in destination if needed ** -- that is not exist -- we know it is a directory
        # should be later ? normally dst is a directory

        self.app_state.count_dir       += 1

        if not os.path.isdir(dst):
            self.app_state.count_dir_added      += 1
            if not( self.app_state.simulate_mode_flag ):
                os.mkdir( dst )    #want to check for success??? *todo
        else:
            self.app_state.count_dir_updated      += 1

        self.app_state.post_to_tk_queue( ">display_dirs", "__msg" )    # is this were function should live ??

        for i_name in names:
            # file from / file to
            srcname = os.path.join( src, i_name )
            dstname = os.path.join( dst, i_name )

            while self.app_state.pause_flag:
                time.sleep( self.parameters.pause_sleep )

            if self.explore_if_dir( srcname, dstname ):
                continue     # done with this name

            if self.process_filter( src, dst, srcname, dstname ):  #  name, src, dst, srcname, dstname
                continue

            self.explore_file(  srcname, dstname )

    # ---------------------------------------
    def explore_file( self, srcname, dstname ):
        pass

    # ---------------------------------------
    def explore_if_dir( self, srcname, dstname ):
        pass


    # !! this may log or post to text area too many times
    # ---------------------------------------
    def logDetailLine( self, aMsg, logger_level = None ):
        """
        thread and gui not at same time - really? !!
        log and post to the main thread, when posted there
        it may log to the gui
        """
        # aMsg may not be a string
        msg_ascii    = aMsg.encode( "ascii", "backslashreplace" ).decode(  )
        if self.logDetailFile != None:
             self.logDetailFile.write( msg_ascii + "\n" )   # was \r\n
#        print(aMsg)
        self.app_state.post_to_tk_queue( ">text", msg_ascii )
        if logger_level != None:
            self.logger.log( logger_level, msg_ascii, )

    # ---------------------------------------
    def logSummaryLine( self, aMsg, logger_level = None ):
        msg_ascii    = aMsg.encode( "ascii", "backslashreplace" ).decode(  )
        if self.logSummaryFile != None:
            self.logSummaryFile.write( msg_ascii + "\n" )   # was \r\n
        print( aMsg )
        self.app_state.post_to_tk_queue( ">status", msg_ascii )
        if logger_level != None:
            self.logger.log( logger_level, msg_ascii, )

    # ---------------------------------------
    def logBothLine( self, aMsg, logger_level = None ):

        self.logDetailLine(      aMsg )
        self.logSummaryLine(     aMsg )
        if logger_level != None:
            msg_ascii    = aMsg.encode( "ascii", "backslashreplace" ).decode(  )
            self.logger.log( logger_level, msg_ascii, )

    # ---------------------------------------
    # do a test on this
    # try removing read only on target # makeW = 1 to make target writeable
    def copyFile( self, aSrcName, aDstName ) :
        """
        backup thread only
        """
        try:
                shutil.copy2( aSrcName, aDstName )

        except ( IOError, os.error ) as why:
                msg = "Retry copy %s to %s: %s" % (repr(aSrcName), repr(aDstName), str(why))
                self.logDetailLine( msg )
                #myAppGlobal.errorCount  += 1
                # ?? make sure file closed first?
                self.copyFileMakeW( aSrcName, aDstName )

    # ------------------------------------------
    def copyFileMakeW( self, aSrcName, aDstName ) :
        """
        called from copyFile as a backup method
        try removing read only on target # makeW = 1 to make target writeable
        """
        try:
                #or can we delete it?
                os.chmod( aDstName, stat.S_IWRITE)
                shutil.copy2( aSrcName, aDstName )
                # ?? consider changing it back

        except (IOError, os.error) as why:
                msg = "Can't copyMakeW %s to %s: %s" % (repr(aSrcName), repr(aDstName), str(why))
                self.logDetailLine( msg, logging.ERROR  )
                self.app_state.increment_error_cnt( msg )

    # =================================


#    #-----------------------------------------------
#    def shorten_path( self a_path ):
#
#        pass





    #-----------------------------------------------
    def validate_dir_minus_n( self, a_dir, a_minus, message ):
        """
        in this case the dir that must exist and match are up one level
        from source and dest -- the dest top level directory may be created
        in the course of the backup
            a_dir,
            a_minus    int, how menu levels we should move up in the dir structure,
            message    string, name for this dir like destDir.....
        Return:
            True if dir is valid
        """
        test_dir     = a_dir
        if a_minus > 0:
            parts     = pathlib.PureWindowsPath( a_dir ).parts
            if len( parts ) <= a_minus:
                msg =  f"error> {message} = >>{a_dir}<< has too few parts, need {a_minus}"
                self.logDetailLine( msg )
                self.app_state.increment_error_cnt( msg )
                return False
            test_dir      = os.path.join( *parts[0:-a_minus] )

        if not os.path.exists( test_dir ):
            if a_minus >0:
                msg =  f"error> {message} minus {a_minus} = >>{a_dir}<< does not exist"
            else:
                msg =  f"error> {message}  = >>{a_dir}<< does not exist"
            self.logDetailLine( msg )
            self.app_state.increment_error_cnt( msg )
            return False
        return True

    #-----------------------------------------------
    def validate_backup( self ):
        """
        check that all the parameters set up for copy are ok
        """
        self.logger.info( "validate_backup" )

        if ( self.app_state.destDir == self.app_state.sourceDir ):
                msg =  "error> destDir = sourceDir"
                self.logDetailLine( msg )
                self.app_state.increment_error_cnt( msg )
                return False

        if not self.validate_dir_minus_n( self.app_state.destDir, self.app_state.minus_dir, "destDir"  ):
                # messages and logging in function
                return False

        # replaced by above
#        if not os.path.exists( self.app_state.destDir ):
#                msg =  "error> destDir does not exist"
#                self.logDetailLine( msg )
#                self.app_state.increment_error_cnt( msg )
#                return False

        if not os.path.exists( self.app_state.sourceDir ):
                msg  = "error> sourceDir does not exist"
                self.logDetailLine( msg )
                self.app_state.increment_error_cnt( msg )
                return False

        if not os.path.isdir( self.app_state.sourceDir ):
                msg  = "error> sourceDir not a dir"
                self.logDetailLine( msg )
                self.app_state.increment_error_cnt( msg )
                return False
        if self.app_state.minus_dir == 0:
            if (not os.path.isdir( self.app_state.destDir )):   # could alread exist as a file
                    msg  = "error> destDir not a dir"
                    self.logDetailLine(msg )
                    self.app_state.increment_error_cnt( msg )
                    return False
        else:
            pass
            # perhaps another test

        destTail        = os.path.split( self.app_state.destDir   )[1]     # may be a tail function
        sourceTail      = os.path.split( self.app_state.sourceDir )[1]     # may be a tail function

        #print "tail check"
        self.logDetailLine( sourceTail + " => " + destTail )

        if sourceTail != destTail:
                msg  =  "error> source and dest tails do not match"
                self.logDetailLine( msg )
                self.app_state.increment_error_cnt( msg )
                return False

#        print("mediaid check")
        if self.validatMediaId( self.app_state.sourceDir, self.app_state.sourceMediaId )  != 1:
            # ?? really why is this already done do not get this russ ??
            # error all ready logged and counted
            # msg  =  "error> source and dest tails do not match"
            # myAppGlobal.logDetailLine( msg )
            # myAppGlobal.erMsg  = msg
            # myAppGlobal.errorCount  += 1
            return False

        if self.validatMediaId( self.app_state.destDir, self.app_state.destMediaId )  != 1:
            # error all ready logged and counted
            # msg  =  "error> source and dest tails do not match"
            # myAppGlobal.logDetailLine( msg )
            # myAppGlobal.erMsg  = msg
            # myAppGlobal.errorCount  += 1
            return False

        if not( self.validate_filter() ):
            return False

        # if here we are ok
        print("validate_backup return True", flush = True)
        return True

#----------------------
    def validate_filter( self ):
        """
        try to check that filters are ok, not really implemented
        if used abstract base class this might assist
        """
        filter_ok      = True  # may itterate 0 times
        filter_objects = self.app_state.filter_objects

        for i_filter_object in filter_objects:
             if not( self.validate_one_filter(  i_filter_object ) ):
                   filter_ok = False
                   continue

        return filter_ok

    #-----------------------------------------------
    def validate_one_filter( self, filter ):
        """
        try to see if filter is ok
        ran into trouble when valid file is required
        ?? delete
        !! complete it
        """
#        # test filter function with try except
#        # return True if ok
#        try:
#            filter.check_file( "name", "src", "dest")
#        except:
#            msg  = "error> filter function failed"
#            self.logDetailLine(msg )
#            self.app_state.er_msg = msg
#            self.app_state.error_cnt  += 1
#            return False
#        return True
        return True

    # =================================
    def validatMediaId( self, aTargetDir, aMediaId ):
            """
            print "in mediaid check"
            #os.stat(path)

            # or just first couple of char?
            #Perform a stat() system call on the given path.
            #The return value is an object whose attributes correspond to the members of
            #the stat structure, namely: st_mode (protection bits), st_ino (inode number),
            #st_dev (device),
            """
            ok = 1    # the default return code

            try:
                # print "try ignore"
                if aMediaId != "ignore":
                        # print "media id != ignore is  " + aMediaId
                        # extract the drive letter
                        fileName    = getRoot( aTargetDir )+ "MediaId.txt"
                        file        = open( fileName )
                        data        = file.readline()
                        data        = data[ 0:len(data) - 1 ] #get rid of newline at end

                        if data == aMediaId:
                                ok = 1
                                #print "media id ok"
                        else:
                                msg = "MediaId " + aMediaId + " <> " + data
                                self.logDetailLine( msg )
                                self.app_state.increment_error_cnt( msg )
                                print(msg)
                                ok = 0
                else:
                        # media id is ignore
                        pass
                        #print "media id" + aMediaId

            except (IOError, os.error) as why:
                msg = "Cant read first line of " +  fileName + "  " + str(why)
                self.erMsg  = msg
                self.logDetailLine( msg )
                self.app_state.increment_error_cnt( msg )
                self.logger.critical( msg )
                ok = 0

            return ok

   # ----------------------------------
    def open_logs( self ):
        """
        call from main thread only
        """
        print("open logs")
        self.logDetailFile         = open( self.app_state.log_detail_fn,  "a" )
        #print "openDetailLog"
        self.logSummaryFile        = open( self.app_state.log_summary_fn, "a" )
        #print "openSummaryLog in dir backup"

   # ----------------------------------
    def close_logs( self ):
        """
        call backup thread only
        """
        print( "close logs, still called any more ??" )
        sys.stdout.flush()
        if self.logDetailFile  != None:
            self.logDetailFile.close( )
            self.logDetaiFile    = None

        if  self.logSummaryFile != None:
            self.logSummaryFile.close( )
            self.logSummaryFile  = None

# =================================
def   getRoot( fileName ):
        # may depend on os or something sim,  this is for windows
        # file name may be a file or dir path
        return fileName[0:3]

# =================================
# modified date is really a long here

def ModifiedDate(file1):
    # 'purpose is to report date file last modified  '
    stats=os.stat(file1)
    #print "stats are",file1,stats[8]
    modified=stats[8]

    #modified=os.stat(file1)[ST_MTIME]

    return modified


# =================================

if __name__ == "__main__":
    print("")
    print("================= do not run me please, I have nothing to say  ===================")




