# -*- coding: utf-8 -*-


# gui for backup

"""
display log skipped
debug level
"""



import tkinter as Tk
import logging

# ------- local -------------

from   app_global import AppGlobal

class GUI( ):
    # --------------------------------- init and make gui --------------------
    def __init__( self,   ):

        AppGlobal.gui       = self
        self.controller     = AppGlobal.controller
        self.app_state      = AppGlobal.app_state
        self.parameters     = self.controller.parameters

        self.logger         = logging.getLogger( AppGlobal.logger_id + ".gui" )   # assign to logger or get all the time does logger have aname or does this assign

        self.logger.debug( "in class GUI init" )

        self.root           = Tk.Tk()

        self.text_log       = None    # log area for text created later

        title        =  self.controller.app_name + ": version = " + self.controller.app_version
        self.root.title( title )

        # ----- set up root for resizing
        self.root.grid_rowconfigure(    0, weight=0 )
        self.root.grid_columnconfigure( 0, weight=1 )

        self.root.grid_rowconfigure(    1, weight=1 )
        self.root.grid_columnconfigure( 1, weight=1 )

        # ----- frame0
        frame_top = Tk.Frame( self.root )

        #frame0.grid_rowconfigure(    0, weight=1 )
        #frame0.grid_columnconfigure( 0, weight=1 )

        frame_top.grid_rowconfigure( 0, weight=0 )

        frame_top.grid(row=0, column=0,  columnspan=2, sticky = Tk.N + Tk.S + Tk.E + Tk.W )
        #frame_top.grid( columnspan=2, sticky=W )

        # ----- frame0
        frame0 = Tk.Frame( self.root )
        #frame0.grid_rowconfigure(    0, weight=1 )
        frame0.grid_columnconfigure( 1, weight=1 )

        frame0.grid( row=1, column=0, sticky = Tk.N + Tk.S + Tk.E + Tk.W )

        self.make_log_area( frame0 )
        self.make_top( frame_top )
        # where STATE=  The button state: NORMAL, ACTIVE or DISABLED. Default is NORMAL.
        #self.text_in.config(state=Tk.NORMAL)
        self.ix_text_log_lines     = 0 # count lines written to detail

        #self.write_log_text( "test write_log_text")
#        self.display_app_state_parms( )
        self.root.geometry( self.parameters.root_geometry )

    # ------------------------------------------
    def run( self, ):
        """
        run the giu
        call from controller -- ususal mainloop
        """
        self.display_app_state_parms( )
        print( "self.root.mainloop()>>", flush = True)
        self.root.mainloop()

    # ------------------------------------------
    def make_top( self, parent_frame,  ):
        """
        gui creation
        top of window, mostly parameter display and buttons
        passed a resizable frame, where we place our controlls
        caller places the frame
        """
#        print( "make_top" ) ;  sys.stdout.flush()

        parent_frame.grid_rowconfigure(    0, weight=1 )
        parent_frame.grid_columnconfigure( 0, weight=0 )

        # displace various stuff about status start just named with numbers, fixed later
        self.lables    = []    # these are the gui lables, not the text content
        for ix in range( 1, 13 ):
            lab_msg = Tk.Label( parent_frame, text = str( ix ))
            lab_msg.grid( row=ix,  column=1 , sticky = Tk.W   )
            lab_msg.config( width = 80)
            self.lables.append( lab_msg )

        a_button = Tk.Button( parent_frame , width=10, height=2, text = "Summary" )
        a_button.grid( row=1, column=0, sticky = Tk.W    )
        a_button.config( command = self.controller.os_open_summary  )

        a_button = Tk.Button( parent_frame , width=10, height=2, text = "Detail" )
        a_button.grid( row=2, column=0, sticky = Tk.W    )
        a_button.config( command = self.controller.os_open_detail  )

        a_button = Tk.Button( parent_frame , width=10, height=2, text = "PyLog" )
        a_button.grid( row=3, column=0, sticky = Tk.W    )
        a_button.config( command = self.controller.os_open_pylog  )

        a_button = Tk.Button( parent_frame , width=10, height=2, text = "Start" )
        a_button.grid( row=5, column=0, sticky = Tk.W    )
        # a_button.bind("<Button-1>", self.controller.start_object_thread )
        a_button.config( command = self.controller.start_object_thread_bcb  )
        a_button.config( state=Tk.DISABLED )   #  NORMAL  !! does not seem to work
        self.button_start  = a_button

        #e1.grid( row=0, column=1, sticky = Tk.N + Tk.S + Tk.E + Tk.W )
        a_button = Tk.Button( parent_frame , width=10, height=2, text = "Pause" )
        a_button.grid( row=6, column=0, sticky = Tk.W    )
        #a_button.bind("<Button-1>", self.controller.button_says_pause )
        a_button.config( command = self.controller.pause_bcb  )
        self.button_pause   = a_button

    # ------------------------------------------
    def make_log_area( self, parent_frame,  ):
        """
        the logging area (mostly a scrolling text area ) now at the bottom
        passed a resizable frame, where we place our controlls
        caller places the frame
        """
        parent_frame.grid_rowconfigure(    0, weight=1 )
        #parent_frame.grid_columnconfigure( 0, weight=0 )

        #parent_frame.grid_rowconfigure(    0, weight=0 )
        parent_frame.grid_columnconfigure( 1, weight=3 )

        # button frame
        bframe  = Tk.Frame( parent_frame, bg = "black", width=30  ) # width=300, height=800, bg ="blue", relief=RAISED, borderwidth=1,  )
        bframe.grid( row=0, column=0, sticky = Tk.N + Tk.S )

        text0 = Tk.Text( parent_frame , width=50, height=20 )

        s_text0 = Tk.Scrollbar( parent_frame  )  # LEFT left
        s_text0.grid( row=0, column=2, sticky = Tk.N + Tk.S )

        s_text0.config( command=text0.yview )

        text0.config( yscrollcommand=s_text0.set )

        text0.grid( row=0, column=1, sticky = Tk.N + Tk.S + Tk.E + Tk.W  )

        self.text_log  = text0   # not very ellegant

        # spacer
        s_frame = Tk.Frame( bframe, bg ="green", height=20 ) # width=30  )

        s_frame.grid( row=0, column=0  )

    # --------------------------------- end of make gui, begin runtime functions --------------------
    # ------------------------------------------
    def display_app_state_parms( self ):
        """
        pretty dumb displays all of them
        also inits variables for later use, so init them all even if with blanks
        """
        app_state   = AppGlobal.app_state
        print( app_state.backup_name, flush = True )

        ix = 1
        lab   =  app_state.backup_name   #     + " media id: " + self.app_state.sourceMediaId
        self.display_parm( ix,  lab )

        ix += 1
        lab   = f"Source:  { app_state.sourceDir  }  Media Id:  { app_state.sourceMediaId } "
        self.display_parm( ix,  lab )

        ix += 1
        lab   = "Dest:    " + app_state.destDir       + "     Media Id: " + app_state.destMediaId
        self.display_parm( ix,  lab )

        ix += 1
        lab   = "Log summary:    " + app_state.log_summary_fn       + "      Log detail: " + app_state.log_detail_fn
        self.display_parm( ix,  lab )

        ix += 1
        lab   = "Simulate copy:    " + str( app_state.simulate_mode_flag )
        self.display_parm( ix,  lab )

#        ix += 1
#        lab   = "filter function :  ??????? " #  + self.get_filter_text()
#        self.display_parm( ix,  lab )

        ix += 1 # space for filter text
        self.ix_filter_text  = ix
        self.display_app_state_filter_text( )

        ix += 1 # space for filter text
        self.ix_status_text  = ix
        self.display_app_state_status( )

        # ( display_dirs   ix_display_dirs )
        ix += 1
        self.ix_display_dirs  = ix
        self.display_dirs( )

        # ( display_files  ix_display_files  )
        ix += 1
        self.ix_display_files  = ix
        self.display_files( )

        # ( display_bytes   ix_display_bytes )
        ix += 1
        self.ix_display_bytes  = ix
        self.display_bytes( )

        # ( display_errors ix_display_errors )
        ix += 1
        self.ix_display_errors  = ix
        self.display_errors( )



 #
#        ix += 1
#        self.ix_error_text  = ix
#        self.display_app_state_error()


# ==================== end construction ====================
#    right now we identify by number, since all the numbers are here so far we
#    do not make symbolic

    #----------------------------------------------------------------------
    def display_app_state_status(self, ):
        """
        another display update
        """
        self.lables[ self.ix_status_text ].config( text = "Status: " + self.app_state.status )

    #----------------------------------------------------------------------
    def display_files(self,  ):
        """
        another display update
        """
        self.lables[ self.ix_display_files ].config( text = (
                   f"Files total :  {   self.app_state.count_total   }   added: { self.app_state.count_added  }   updated: {   self.app_state.count_updated }" +
                   f" skipped: { self.app_state.count_skipped }" )  )

    #----------------------------------------------------------------------
    def display_bytes(self,  ):
        """
        write the bytes copied .....
        """
        self.lables[ self.ix_display_bytes ].config( text =
                   f"Bytes copied: {mega_etc( self.app_state.bytes_copied ) }   bytes skippped: { mega_etc( self.app_state.bytes_skipped ) }" )

#    #----------------------------------------------------------------------
#    def display_app_state_count(self,  ):
#        """
#        write the count and bytes copied
#        """
#        self.lables[ self.ix_count_text ].config( text =
#                   f"count:  {mega_etc( self.app_state.total_cnt ) } bytes copied: {mega_etc( self.app_state.bytes_copied)}" )

    #----------------------------------------------------------------------
    def display_dirs(self,  ):
        """
        another display update
        """
        text = f"Directories total:  { self.app_state.count_dir  } added: {self.app_state.count_dir_added } updated: {self.app_state.count_dir_updated }"
                  # " skipped: {self.app_state.count_dir_updated }" )
        self.display_parm( self.ix_display_dirs, text  )

    #----------------------------------------------------------------------
    def display_app_state_filter_text(self,  ):
        """
        another display update
        include directory depth  max_dir_depth
        """
        text = f"Max Dir Depth: {self.app_state.max_dir_depth}  Filters: { self.app_state.get_filter_text() } "
        self.display_parm( self.ix_filter_text, text  )

    #----------------------------------------------------------------------
    def display_errors( self, ):
        """
        another display update
        """
        lab   = f"Error count: {self.app_state.count_errors}  Last err message: { self.app_state.error_msg  }"
        self.display_parm( self.ix_display_errors,  lab )

    #----------------------------------------------------------------------
#    def display_app_state_error( self, ):
#        lab   = f"error count: { str( self.app_state.count_errors ) } last err message: { self.app_state.error_msg  }"
#        #self.display_parm( self.ix_error_text,  lab )

#    #----------------------------------------------------------------------
#    def display_bytes( self, ):
#        text = f" bytes = { 0 - 1 } "
#        self.display_parm( self.ix_display_bytes, text  )

    #----------------------------------------------------------------------
    def write_text(self, string):
        """
        write to text log area
        """
        # self.ix_text_log_lines   += 1
        self.ix_text_log_lines   += 1
        # erase when max is hit
        if self.ix_text_log_lines > self.parameters.max_detail_lines:
            self.text_log.delete( 1.0, Tk.END )
            self.ix_text_log_lines   = 0

        self.text_log.insert( Tk.END, string )
        self.text_log.see(    Tk.END )          # add scrolling  -- option to turn off

    #----------------------------------------------------------------------
    def display_parm( self, gui_ix, parm_value ):
        """
        this is xxx case base, have individual call for each better??
        see directory_backup.display_parms( self ) which is an initializer and should probably be moved here.
        """
        self.lables[ gui_ix ].config( text = parm_value )

# =================================
def  mega_etc( aNum ):
        """
        early code probably not best
        """
        if aNum > 1000000000:
                aNum    = aNum / 1000000000.
                rStr    = str( aNum ) + " GigaB"

        elif aNum > 1000000:
                aNum    = aNum / 1000000.
                rStr    = str( aNum ) + " MegaB"

        elif aNum > 1000:
                aNum    = aNum / 1000.
                rStr    = str( aNum ) + " KiloB"

        else:
                rStr    = f"{aNum} B"
        return rStr


# ==================================================
if __name__ == "__main__":
    pass
#    test = GUI( None )
#    test.root.mainloop()
    #app = App( None )
    #app.mainloop()


# ========================= eof =========================

