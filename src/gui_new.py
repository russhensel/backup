# -*- coding: utf-8 -*-


"""
this gui now runs, not great but better

"""
import logging
from   tkinter import *   # is added everywhere since a gui assume tkinter name space
#from   tkinter import ttk
import sys
#from   tkcalendar import Calendar, DateEntry
#import datetime
#from   tkinter.filedialog import askopenfilename
import pyperclip
import ctypes

#------- local imports
from   app_global import AppGlobal


# ======================= begin class ====================
class GUI:
    """
    gui for the application
    """
    def __init__( self,  ):
        """
        build the application GUI
        """
        AppGlobal.gui           = self

        self.controller         = AppGlobal.controller
        self.parameters         = AppGlobal.parameters
        self.app_state          = AppGlobal.app_state
        self.root               = Tk()

        a_title   = self.controller.app_name + " version: " + self.controller.version + " Mode: " +self.parameters.mode

        self.root.title( a_title )

        self.root.geometry( self.parameters.win_geometry )

        if self.parameters.os_win:
            # from qt - How to set application's taskbar icon in Windows 7 - Stack Overflow
            # https://stackoverflow.com/questions/1551605/how-to-set-applications-taskbar-icon-in-windows-7/1552105#1552105

            icon = self.parameters.icon
            if not( icon is None ):
                print( "set icon "  + str( icon ))
                ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(icon)
                self.root.iconbitmap( icon )
            else:
                print( "no icon for you!"  + str( icon ))

        self.logger             = logging.getLogger( self.controller.logger_id + ".gui")
        self.logger.info("in class gui_new GUI init") # logger not currently used by here

        # next leftover values may not be used
        self.save_redir          = None
        self.save_sys_stdout     = sys.stdout

        self.max_lables          = 6   # number of labels, normally used for parameters
        self.lables              = []  # labels normally for parameters

        self.rb_var              = IntVar()

        #------ constants for controlling layout and look  ------
        self.button_width         = 6

        self.button_padx          = "2m"
        self.button_pady          = "1m"

        self.btn_color            = self.parameters.btn_color
        self.bkg_color            = self.parameters.bkg_color

        next_frame       = 0      # index of frames and position row for frames

        self.root.grid_columnconfigure( 0, weight=1 ) # final missing bit of magic

        a_frame = self._make_sys_frame( self.root,  )
        a_frame.grid(row=next_frame, column=0, sticky=E + W + N)
        next_frame += 1

        a_frame = self._make_app_frame( self.root,  )
        a_frame.grid(row=next_frame, column=0, sticky = E + W + N)
        next_frame += 1

        a_frame = self._make_app_log_frame( self.root,  )
        a_frame.grid(row=next_frame, column=0, sticky = E + W + N)
        next_frame += 1

        a_frame = self._make_message_frame( self.root,  )
        a_frame.grid(row=next_frame, column=0, sticky = E + W + N + S)
        next_frame += 1

        self.root.grid_columnconfigure( 0, weight=1 )
        self.root.grid_rowconfigure(    0, weight=0 )
        self.root.grid_rowconfigure( ( next_frame - 1 ), weight=1 )

#        #        # -------- does this help
#        self.root.grid_rowconfigure( ( next_frame - 1 ), weight=1 )

    #------ build frames  ------------------------

    # ------------------------------------------
    def run( self, ):
        """
        run the giu -- from old gui, may or may not be right
        call from controller -- usual mainloop
        """
#        self.display_app_state_parms( )
#        print( "self.root.mainloop()>>", flush = True)
        self.root.mainloop()


    # ------------------------------------------
    def _make_app_frame( self, parent,  ):
        """
        gui creation
        top of window, mostly parameter display and buttons
        passed a re-sizable frame, where we place our controls
        caller places the frame
        """
        a_frame  = Frame( parent, width=300, height=200, bg = self.parameters.id_color, relief=RAISED, borderwidth=1 )

        a_button = Button( a_frame, width=10, height=2, text = "Start" )
        a_button.grid( row=5, column=0, sticky = W    )
        # a_button.bind("<Button-1>", self.controller.start_object_thread )
        a_button.config( command = self.controller.start_object_thread_bcb  )
        a_button.config( state=DISABLED )   #  NORMAL  !! does not seem to work
        self.button_start  = a_button

        #e1.grid( row=0, column=1, sticky = Tk.N + Tk.S + Tk.E + Tk.W )
        a_button = Button( a_frame , width=10, height=2, text = "Pause" )
        a_button.grid( row=6, column=0, sticky = W    )
        #a_button.bind("<Button-1>", self.controller.button_says_pause )
        a_button.config( command = self.controller.pause_bcb  )
        self.button_pause   = a_button


        # displace various stuff about status start just named with numbers, fixed later
        self.lables    = []    # these are the gui labels, not the text content
        for ix in range( 1, 13 ):
            init_text  = str( ix )   # use for debug else
            init_text  = ""          # comment out for debug
            lab_msg = Label( a_frame, text = init_text )
            lab_msg.grid( row = ix,  column = 1, sticky = W  + E   )    # does not make it expand all to right, use excess width belwo
            lab_msg.config( width = 300,  anchor="w" )   #  w must be lower /  want enough width to go all the way right
            self.lables.append( lab_msg )

        return a_frame

    # ------------------------------------------
    def _make_app_log_frame( self, parent,  ):
        """
        gui creation
        this is for the log file access buttons
        caller places the frame
        """
        a_frame  = Frame( parent, width=300, height=200, bg = self.parameters.id_color, relief=RAISED, borderwidth=1 )

        a_button = Button( a_frame, width=10, height=2, text = "Summary" )
        a_button.grid( row=0, column=0, sticky = W    )
        a_button.config( command = self.controller.os_open_summary  )

        a_button = Button( a_frame, width=10, height=2, text = "Detail" )
        a_button.grid( row=0, column=1, sticky = W    )
        a_button.config( command = self.controller.os_open_detail  )

        return a_frame

    # ------------------------------------------
    def _make_label( self, a_frame, a_row, a_col, a_text, ):
        """
        return tuple -- or by ref do not need to , test this in templates
        return label
        increment row col
        """
        a_row    += 1
        if a_row >= 2:
                a_row   =  0
                a_col   += 1

        a_label   = ( Label( a_frame, text = a_text, relief = RAISED,  )  )
        a_label.grid( row=a_row, column=a_col, sticky=E + W + N + S )    # sticky=W+E+N+S  )   # relief = RAISED)

        # set weight??
        return ( a_row, a_col, a_label )

    # ------------------------------------------
    def _make_sys_frame( self, parent, ):
            """
            make a test frame place for test stuff esp. buttons
            """
            a_frame  = Frame( parent, width=300, height=200, bg = self.parameters.id_color, relief=RAISED, borderwidth=1 )

            a_button = Button( a_frame , width=10, height=2, text = "Edit Log" )
            a_button.config( command = self.controller.os_open_pylog )
            a_button.pack( side = LEFT )

            a_button = Button( a_frame , width=10, height=2, text = "Edit Parms" )
            a_button.config( command = self.controller.os_open_parmfile )
            a_button.pack( side = LEFT )

            a_button = Button( a_frame , width=10, height=2, text = "Help" )
            a_button.config( command = self.controller.os_open_helpfile )
            a_button.pack( side = LEFT )

            a_button = Button( a_frame , width=10, height=2, text = "About" )
            a_button.config( command = AppGlobal.about )
            a_button.pack( side = LEFT )

            return a_frame

    # ------------------------------------------
    def _make_message_frame( self, parent,  ):
        """
        a frame with scrolling text area and controls for it
        -- there is a scrolled_tet control, not currently using it --- why??
        """
        self.max_lines      = 500
        self.cb_scroll_var  = IntVar()  # for check box in message frame
        color   = "red"
        iframe  = Frame( parent, width=300, height=800, bg ="blue", relief=RAISED, borderwidth=1,  )

        bframe  = Frame( iframe, bg ="black", width=30  ) # width=300, height=800, bg ="blue", relief=RAISED, borderwidth=1,  )
        bframe.grid( row=0, column=0, sticky = N + S )

        text0 = Text( iframe , width=50, height=20 )

        s_text0 = Scrollbar( iframe  )  # LEFT left
        s_text0.grid( row=0, column=2, sticky = N + S )

        s_text0.config( command=text0.yview )
        text0.config( yscrollcommand=s_text0.set )

        text0.grid( row=0, column=1, sticky = N + S + E + W  )

        self.rec_text  = text0

        iframe.grid_columnconfigure( 1, weight=1 )
        iframe.grid_rowconfigure(    0, weight=1 )

        # spacer
        s_frame = Frame( bframe, bg ="green", height=20 ) # width=30  )
        s_frame.grid( row=0, column=0  )
        row_ix   = 0

        # ------ Clear button
        b_clear = Button( bframe , width=10, height=2, text = "Clear" )
        b_clear.bind( "<Button-1>", self.do_clear_button )
        b_clear.grid( row=row_ix, column=0   )
        row_ix   += 1

        #-----
        b_temp = Button( bframe , width=10, height=2, text = "copy sel"
                        )
        b_temp.bind( "<Button-1>", self.doButtonText )
        b_temp.grid( row=row_ix, column=0   )
        row_ix   += 1

        #-----
        b_copy = Button( bframe , width=10, height=2, text = "copy all" )
        b_copy.bind( "<Button-1>", self.do_copy_button )
        b_copy.grid( row=row_ix, column=0   )
        row_ix += 1

        # -------------
        a_widget = Checkbutton( bframe,  width=7, height=2, text="A Scroll", variable=self.cb_scroll_var,  command=self.do_auto_scroll )
        a_widget.grid( row=row_ix, column=0   )
        row_ix += 1

        self.cb_scroll_var.set( self.parameters.default_scroll )

        return iframe

   # ------------------------------------------
    def display_app_state_parms( self ):
        """
        pretty dumb displays all of them
        also inits variables for later use, so init them all even if with blanks
        """
#        print( "implement this" )
#        return

        app_state   = AppGlobal.app_state
        print( app_state.backup_name, flush = True )

        ix = 0
        lab   =  app_state.backup_name   #     + " media id: " + self.app_state.sourceMediaId
        self.display_parm( ix,  lab )

        ix += 1
        lab   = f"Source:  { app_state.sourceDir  }  Media Id:  { app_state.sourceMediaId } "
        self.display_parm( ix,  lab )

        ix += 1
        lab   = "Dest:      " + app_state.destDir       + "     Media Id: " + app_state.destMediaId
        self.display_parm( ix,  lab )

        ix += 1
        lab   = "Log summary:    " + app_state.log_summary_fn       + "      Log detail: " + app_state.log_detail_fn
        self.display_parm( ix,  lab )

        ix += 1
        lab   = "Simulate copy:    " + str( app_state.simulate_mode_flag )
        self.display_parm( ix,  lab )

#        ix += 1
#        lab   = "filter function :  ??????? " #  + self.get_filter_text()
#        self.display_parm( ix,  lab )

        ix += 1 # space for filter text
        self.ix_filter_text  = ix
        self.display_app_state_filter_text( )

        ix += 1 # space for filter text
        self.ix_status_text  = ix
        self.display_app_state_status( )

        # ( display_dirs   ix_display_dirs )
        ix += 1
        self.ix_display_dirs  = ix
        self.display_dirs( )

        # ( display_files  ix_display_files  )
        ix += 1
        self.ix_display_files  = ix
        self.display_files( )

        # ( display_bytes   ix_display_bytes )
        ix += 1
        self.ix_display_bytes  = ix
        self.display_bytes( )

        # ( display_errors ix_display_errors )
        ix += 1
        self.ix_display_errors  = ix
        self.display_errors( )


# ==================== end construction ====================
#    right now we identify by number, since all the numbers are here so far we
#    do not make symbolic

    #----------------------------------------------------------------------
    def display_app_state_status(self, ):
        """
        another display update
        """
#        print( "implement this" )
#        return
        self.lables[ self.ix_status_text ].config( text = "Status: " + self.app_state.status )

    #----------------------------------------------------------------------
    def display_files(self,  ):
        """
        another display update
        """
#        print( "implement this" )
#        return
        self.lables[ self.ix_display_files ].config( text = (
                   f"Files total :  {   self.app_state.count_total   }   added: { self.app_state.count_added  }   updated: {   self.app_state.count_updated }" +
                   f" skipped: { self.app_state.count_skipped }" )  )

    #----------------------------------------------------------------------
    def display_bytes(self,  ):
        """
        write the bytes copied .....
        """
#        print( "implement this" )
#        return
        self.lables[ self.ix_display_bytes ].config( text =
                   f"Bytes copied: {mega_etc( self.app_state.bytes_copied ) }   bytes skippped: { mega_etc( self.app_state.bytes_skipped ) }" )

#    #----------------------------------------------------------------------
#    def display_app_state_count(self,  ):
#        """
#        write the count and bytes copied
#        """
#        self.lables[ self.ix_count_text ].config( text =
#                   f"count:  {mega_etc( self.app_state.total_cnt ) } bytes copied: {mega_etc( self.app_state.bytes_copied)}" )

    #----------------------------------------------------------------------
    def display_dirs(self,  ):
        """
        another display update
        """
#        print( "implement this" )
#        return
        text = f"Directories total:  { self.app_state.count_dir  } added: {self.app_state.count_dir_added } updated: {self.app_state.count_dir_updated }"
                  # " skipped: {self.app_state.count_dir_updated }" )
        self.display_parm( self.ix_display_dirs, text  )

    #----------------------------------------------------------------------
    def display_app_state_filter_text(self,  ):
        """
        another display update
        include directory depth  max_dir_depth
        """
#        print( "implement this" )
#        return
        text = f"Max Dir Depth: {self.app_state.max_dir_depth}  Filters: { self.app_state.get_filter_text() } "
        self.display_parm( self.ix_filter_text, text  )

    #----------------------------------------------------------------------
    def display_errors( self, ):
        """
        another display update
        """
        print( "implement this" )
        return
        lab   = f"Error count: {self.app_state.count_errors}  Last err message: { self.app_state.error_msg  }"
        self.display_parm( self.ix_display_errors,  lab )

    #----------------------------------------------------------------------
    def write_text(self, a_string):
        """
        write to text log area -- old call remove
        """
        self.display_info_string(  a_string )

    # ------------------------------------------
    def display_info_string( self, data ):
        """
        add info prefix and new line suffix and show in recieve area
        data expected to be a string, but other stuff has str applied to it
        consider adding auto log
        """
        nl_char   = "\n"
        sdata      = f">>{data}{nl_char}"
        self.display_string( sdata )
        return

    #----------------------------------------------------------------------
    def display_parm( self, gui_ix, parm_value ):
        """
        this is xxx case base, have individual call for each better??
        see directory_backup.display_parms( self ) which is an initializer and should probably be moved here.
        """
        self.lables[ gui_ix ].config( text = parm_value )

    # ---------------------------------------
    def display_string( self, a_string ):
        """
        print to message area, with scrolling and
        delete if there are too many lines in the area
        logging here !!
        """
#        if  AppGlobal.parameters.log_gui_text:
#
#            AppGlobal.logger.log( AppGlobal.parameters.log_gui_text_level, a_string, )
             #AppGlobal.logger.info( a_string )     # not sure about this level

        self.rec_text.insert( END, a_string, )      # this is going wrong, why how
        try:
             numlines = int( self.rec_text.index( 'end - 1 line' ).split('.')[0] )  # !! beware int( None ) how could it happen ?? it did this is new
        except Exception as exception:
        # Catch the custom exception
            self.logger.error( str( exception ) )
            print( exception )
            numlines = 0
        if numlines > self.max_lines:
            cut  = int( numlines/2 )   # lines to keep/remove
            # remove excess text
            self.rec_text.delete( 1.0, str( cut ) + ".0" )  # got a bad index, probably need int above 2.7 > 3.6 bug i think
            #msg     = "Delete from test area at " + str( cut )
            #self.logger.info( msg )

        if self.cb_scroll_var.get():
            self.rec_text.see( END )

        return

    # ----- button actions - this may be to indirect for some actions that go to the controller  ------------------------
    # ------------------------------------------
    def do_restart_button( self, event):
        self.controller.restart()
        return

#    # ------------------------------------------
#    def do_open_button( self, event):
#        self.controller.open_com_driver()
#        return

    # ------------------------------------------
    def do_clear_button( self, event):
        """
        for the clear button
        clear the message area
        """
        self.rec_text.delete( 1.0, END )
        return

    # ------------------------------------------
    def do_copy_button( self, event ):
        """
        copy all text to the clipboard
        """
        data  = self.rec_text.get( 1.0, END )
        pyperclip.copy( data )
        return


    # ------------------------------------------
    def do_auto_scroll( self,  ):
        """
        pass, not needed, place holder  -- may want to add back
        """
        # print "do_auto_scroll"
        # not going to involve controller
        pass
        return

    # ------------------------------------------
    def cb_send_button( self, event ) :  # how do we identify the button    def cb_send_button( self, event ) :  # how do we identify the button
        """
		think dead code
        any send button
        for at least now do the send echo locally ( and crlf or always use locally )
        """
        #identify the control, one of send buttons, get text and send
        control_ix = -1
        for index, i_widget in enumerate( self.sends_buttons ):

            if i_widget == event.widget:
                control_ix = index
                break
        contents = self.sends_data[control_ix].get()

        self.controller.send( contents )
        return

    # --------------- may be left over from terminal check and delete dead stuff !!---------------------------
    def doButtonText( self, event):
        """
		think dead code
        easy to add functions that look at the button text
        but long term do not use
        """
        btext =  event.widget["text"]

        if btext == "Graph":
            self.controller.cb_graph()
            pass

        else:
            msg   = "no action defined for: " + btext
            self.logger.error( msg )
        return

    # ---------------  end of button actions


# =================================
def  mega_etc( aNum ):
        """
        early code probably not best
        """
        if aNum > 1000000000:
                aNum    = aNum / 1000000000.
                rStr    = str( aNum ) + " GigaB"

        elif aNum > 1000000:
                aNum    = aNum / 1000000.
                rStr    = str( aNum ) + " MegaB"

        elif aNum > 1000:
                aNum    = aNum / 1000.
                rStr    = str( aNum ) + " KiloB"

        else:
                rStr    = f"{aNum} B"
        return rStr



# =======================================

# import test_controller # no longer used

#if __name__ == '__main__':
#        """
#        run the app
#        """
#        import smart_plug_graph
#        a_app = smart_plug_graph.SmartPlugGraph(  )


