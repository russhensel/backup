# -*- coding: utf-8 -*-



"""


this file is .........\_sample_backup.py or was copied from it
use it as a template for your own backup files
as shipped it is close ( some adjustment needed ) to test backing up sample subdirectories
in this application

The lead part of the file and directory names are probably unique to my system
adjust to fit yours.



"""

import  sys
import  os
# ------------- local imports
sys.path.append( "D:/Russ/0000/python00/python3/_projects/backup" )   # may need this so python can find your installation

import directory_backup
from   app_global import AppGlobal    # not used in this example but is sometimes required


"""
this file is not the application it is the setup, the real application is tucked away here:
"""
app = directory_backup.App(  )


"""
this is a name that will be displayed on your GUI, it is only for your convenience
"""
app.set_backup_name( r"Sample and test backup file:"   )

"""
both the source and the destination take the same arguments:
    * the source directory
    * the media id -- not explained here ( but is in the help file at
             Backup Help File - OpenCircuits
             http://www.opencircuits.com/Backup_Help_File )
      and has no function if set to "ignore"
      Again to run you will need to adjust for the location of the files on your system
"""

app.set_source( r"D:/Russ/0000/python00/python3/_projects/backup/TestSource",             "ignore" )

app.set_dest(   r"D:/Russ/0000/python00/python3/_projects/backup/TestDest/TestSource",    "ignore" )


"""
set the location of the detail and summary log -- you can default this in your parameters.py and skip this line

"""
app.set_logs(   r"D:/Russ/0000/python00/python3/_projects/backup/Detail.log",          r"D:/Russ/0000/python00/python3/_projects/backup/2019Summary.log",      )   # detail, summary


"""
if set_simulate_mode is set to True, no actual copies are done but you can see what would have been done in the logs
"""
app.set_simulate_mode(     False )   # True to simulate, False really copy

"""
filters return True if a file and directory pass a test that it is ok to copy them
often we use the filter
directory_backup.AllFiles   which returns True for all files.
after we create it we need to add it.

multiple file filters is a feature in process


"""
filter_object  = directory_backup.FFAll( )     # all files is the default ??
print( str( filter_object ) )                  # for debugging
app.add_filter_object(   filter_object )

"""
run it
"""
app.run_gui()

"""
clean up
"""
app.clean_up()   # cant this be moved inside


#======================= Notes: ========================

"""
You might want to keep some notes here


#app.set_source( r"D:\Russ\0000\SpyderP",              "MainPart on 2TorNot2T"         )

#app.set_dest(   r"I:\Test\DeerAnti",                 "Buffalo 3T Disk"               )
#app.set_dest(   r"d:\Temp\Test\Arduino",             "MainPart on 2TorNot2T"         )


"""