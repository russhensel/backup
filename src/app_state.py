# -*- coding: utf-8 -*-

#    consider this to be part of the controller and part of the
#    thread in terms of updating counts etc,


import logging
import queue
import tkinter as Tk
import os
import time

#--------- local imports
#import parameters
import directory_backup
from   app_global import AppGlobal


class AppState( ):

    """
    track the state/progress of the app. globally available, use by threads should be clearer
    typically one thread can set any can read.
    function calls should be documented
    """
    def __init__( self, ):
        """
        """
        AppGlobal.app_state         = self
        self.controller             = AppGlobal.controller

        # self.gui            = controller.gui not defined yet !
        self.parameters             = AppGlobal.parameters

        print(  f"{AppGlobal.logger_id}.{ self.__class__.__name__ }" )
        self.logger                 = logging.getLogger( f"{AppGlobal.logger_id}.{ self.__class__.__name__ }" )

        self.logger.debug( "init app_state" )   #
        print( f"logger level in AppState = {self.logger.level}", flush = True )

        self.queue_to_tk            = queue.Queue( self.parameters.queue_length )   # send strings back to tkinker mainloop here
        self.ix_queue               = 0
        self.ix_queue_max           = self.parameters.ix_queue_max
        # ------------- set these normally ( always ? ) thru functions, from which thread? --------------

        self.state                  = None   # state the app is in see set_state for values and meanings
        self.simulate_mode_flag     = None   # may not be used

        self.worker_thread_run      = False   # False to kill the worker thread, True to let run, for early shutdown not implemented !!

        # USING ERROR COUNT NOT THIS
        #self.go_flag              = 0   # should be set to 1 at end of validate
                                   # and any change that might required validation should set to 0
        self.pause_flag            = False   # 1 will pause in a wait loop

        self.ix_dir_depth          =  0
        self.reset_counts()
        self.set_default_backup_values( )

        # here    = os.getcwd()
        self.initial_dir  = os.getcwd() # this is where the job is kicked off from not where the app lives # assumes built prior to anyone/thing changing dir
        print(  f"current dir  {self.initial_dir}")

    #------------------------------------------
    def info( self, print_info = True):
        """
        """
        info     = self.__dir__()
        if print_info:
            print( info )

        vars_here  = vars( self )
        if print_info:
            print( vars_here )
            #directory_backup.print_list( vars_here )
            directory_backup.print_dict( vars_here )

        return

    # ------------------------------------------
    def reset_counts( self,  ):
        """
        pretty much an init thing, not sure why putting in own function
        update counts with a function, could have used property but this function looks right
        """
        #self.total_cnt              = 0   # total files ( and perhaps some directories processed )
        #self.total_files            = 0   # total just files - if we can do it

        self.count_dir              = 0
        self.count_dir_added        = 0
        self.count_dir_updated      = 0
        self.count_dir_skipped      = 0

        self.count_total            = 0
        self.count_added            = 0
        self.count_updated          = 0
        self.count_skipped          = 0

        self.bytes_copied           = 0
        self.bytes_skipped          = 0

        self.start_ts               = 0    # not valid until we start
        self.end_ts                 = 0    # not valid until done

        self.initial_dir            = None

        self.count_errors           = 0       # copy errors   self.app_state.error_cnt
        self.error_msg              = "no errors"

        self.status                 = "status ok"

    # ------------------------------------------
    def set_default_backup_values( self,   ):
        """
        set to stupid or get from parameters, then the backup script, then parameters for those still at None
        """

        # ---------------- set externally before gui is started
        self.backup_name            = "Un-named backup"

        self.sourceDir              = "source dir"
        self.destDir                = "dest dir"

        self.sourceMediaId          = "ignore"
        self.destMediaId            = "ignore"

        self.log_detail_fn         = self.parameters.log_detail_fn
        self.log_summary_fn        = self.parameters.log_summary_fn

        self.filter_objects        = []    # new for multiple filters get rid of filter_object as on

        self.max_dir_depth         = -1

        self.minus_dir             = 0    # if > 0 then in dest we validated dest, this many directories up  right now only 0 or -1 work

        # logging and simulate
        self.simulate_mode_flag    = self.parameters.simulate_mode_flag   # True, no actually copy simulate

        self.log_skipped_flag      = self.parameters.log_skipped_flag     # False do not log skipped    self.app_state.log_skipped_flag

    # ------------------------------------------
    def set_state( self, new_state,  ):
        """
        think may be called from either thread, depending on state
        problem would come up if two threads tried to call at the same time.
        only valid way to change app state

        ok = self.set_state( "run" )
        # idea ?? actions are mostly but not always new states
        some actually change state some are to be used externally just to make
        sure it is ok and rely on external to do it
        return success
        """
        # ?? possibly check if legal mostly done
        if new_state   == "init":
            # run pause disabled
            self.state   = new_state
            return True

        gui  = AppGlobal.gui
        if new_state == "start_ng":
            self.logger.critical(  "app.state set state to start_ng",  stack_info=True )   # just where I am full trace back most info

            gui.button_start.config( state=Tk.DISABLED  )
            gui.button_pause.config( state=Tk.DISABLED  )
            self.state   = new_state
            return True

        elif new_state == "start_ok":
            # any to => start_ok
            gui.button_start.config( state=Tk.NORMAL    )
            gui.button_pause.config( state=Tk.DISABLED  )
            self.state = new_state
            return True

        elif new_state == "run":

            if self.state == "start_ok":
                #  start_ok => run
                self.pause_flag            = False
                gui.button_start.config( state=Tk.DISABLED  )
                gui.button_pause.config( text='Pause'  )
                gui.button_pause.config( state=Tk.NORMAL  )
                self.state = new_state
                #print "new state run fro start ok "
                self.logger.debug( "new state run from start ok " )
                return True

            elif self.state == "pause":
                # restart from pause => run
                self.pause_flag            = False
                gui.button_start.config( state=Tk.DISABLED  )
                gui.button_pause.config( text='Pause'  )
                gui.button_pause.config( state=Tk.NORMAL    )
                self.state = new_state
                return True

            else:
                # invalid change
                print("invalid change to run")

                self.logger.critical( "invalid attempt to change state from { self.state } to {new_state}",
                                    stack_info=True )   # just where I am full trace back most info
                return False

        elif new_state == "pause":

            if self.state != "run":
                print("invalid change to pause")
                # self.logger.debug( "invalid change to pause from" + self.state )
                self.logger.critical(  "invalid attempt to change state from { self.state } to {new_state}",  stack_info=True )

                return False

            else:
                # run to pause
                self.pause_flag            = True
                gui.button_pause.config( text='EndPause'  )
                gui.button_pause.config( state=Tk.NORMAL  )
                self.state = new_state
                return True

        elif new_state == "done":

            gui.button_start.config( state=Tk.DISABLED  )
            gui.button_pause.config( state=Tk.DISABLED  )
            return True

        else:
            #not valid

            #self.log.critical( "-------in----------" )
            self.logger.critical(  "invalid attempt to change state from { self.state } to {new_state}",  stack_info=True )

            return False

        #self.state = new_state
        return True

    # --------------------------------------------------
    def post_to_tk_queue( self, where, msg ):
        """
        seem not to have a post to helper
        see polling for meaning of where
        call from thread or gui, possibly at the same time ??
        believe messages come from the thread only to the gui??
        msg could be string or number
        where     = "s"  d   b "c"  which mean? look in polling
        this is check in polling in the gui
        loop until post works
        same function  in app_state
        called from backup_thread
        """
        loop_flag      = True   # keep looping while True  -- this seems clumsy, old code
        self.ix_queue  = 0      # count loops this time
        while loop_flag:
            loop_flag       = False
            self.ix_queue  += 1
            try:
                self.queue_to_tk.put_nowait( ( where, msg ) )
            except queue.Full:
                # try again but give polling a chance to catch up
                msg   = "AppState.post_to_tk_queue() queue full looping."
                print( msg )
                self.logger.info( msg )
                # protect against infinit loop if queue is not emptied
                if self.ix_queue > self.ix_queue_max:
                    msg   = "AppState.post_to_tk_queue() too much queue looping"
                    print( msg )
                    self.logger.info( msg )
                    # perhaps time to throw a big exception, here we keep going printing and logging like crazy

                else:
                    loop_flag = True
                time.sleep( self.parameters.tk_queue_sleep )

    # --------------------
    def increment_error_cnt( self, msg ):

        self.count_errors    += 1       # copy errors   self.app_state.error_cnt
        self.error_msg        = msg


        #lab   = "last err message:    " + self.error_msg     + "     count: " + str( self.error_cnt )
        self.post_to_tk_queue( ">display_errors", "" )

#        # --------------------
#        def increment_count_total( self, ):
#        #    def increment_total_cnt( self, ):
#            self.total_cnt        += 1       # copy errors   self.app_state.error_cnt
#            self.post_to_tk_queue( ">display_files", "" )

#    # --------------------
#    def increment_count_skipped( self, ):
#        """
#        another increment function
#        """
#        self.count_skipped        += 1
#        self.post_to_tk_queue( ">display_files", "" )

#    # --------------------
#    def increment_count_dir( self, ):
#        """
#        call from backup_thread
#        """
#        self.count_dir        += 1       # copy errors   self.app_state.error_cnt
#        self.post_to_tk_queue( ">display_dirs", "" )

#    # --------------------
#    def increment_bytes_copied( self, amt ):
#        self.bytes_copied         += amt       # c
#        self.post_to_tk_queue( ">display_files", "" )


    # --------------------------------------------------
    def get_filter_text( self ):

        filter_texts   = []
        filter_objects = self.filter_objects
        # is no filter all files, or do we require at least one filer
        if len( filter_objects ) == 0:
            text = "no filters"

        else:
            for i_filter_object in filter_objects:
                 filter_texts.append( str( i_filter_object ) )
            text  = "\n".join( filter_texts )

        return text

        #return "!! do filter text"


if __name__ == "__main__":
    print("")
    print("================= do not run me ( app_state.py ) please, I have nothing to say  ===================")




# ========================== eof =============================


