#!/usr/bin/python

"""
for info see
    *>text D:\Russ\0000\python00\python3\_projects\backup_wip\readme_rsh.txt



"""

import os
import logging
import threading
import datetime
import time
import queue
import sys
import operator
#import os
#import sys

# ------ local modules -----

import gui
import gui_new
import parameters
import backup_thread
import app_state

from   app_global import AppGlobal

# ================= class =========================
class App( ):

    def __init__( self,  ):
        """
        this is the main controller for the application, it runs in the gui thread, the actual work
        will be controlled from the backup_thread.DirectoryBackup
        correct order to bring up:
            parameters
            logger
            gui
            appState
            backup_thread
        """
        AppGlobal.controller    = self

        self.app_name           = "Directory Backup for Python 3"
        self.name               = self.app_name
        self.app_version        = "2019_11_21.1"
        self.version            = self.app_version   # new std

        self.parameters         = parameters.Parameters(  )

        self.logger             = None     # std  local access to logger set in config_logger()
        self.logger_id          = "app"    # keeping short as obvious
        self.config_logger()

        self.prog_info(  )

        self.app_state          = app_state.AppState( )
		# you can swap in different gui's here as i sometimes do in development
#        self.gui                = gui.GUI( )
        self.gui                = gui_new.GUI( )

        self.set_simulate_mode( False )     # default to False
        self.set_backup_name( self.parameters.default_name  )

        self.polling_delta      = self.parameters.poll_delta_t       # this is a delay time to deliberately slow the program down and reduce resource usage

        self.backup_thread      = backup_thread.BackupThread( )      # this is the object beware of restart !!
        self.worker_thread      = None   # create when we start
        #self.worker_thread_run   = False

        self.init_fun            = self.check_parms               # this is a function that will be run when the gui starts, not now

        self.gui.root.after( self.polling_delta, self.polling_begin )   # set gui polling   !! change to init

        self.logger.info( "App Init Done..." )

    # ------------------------------------------
    def config_logger( self, ):
        """
        configure the python logger
        Return:
            in self.logger  ....
        """
        AppGlobal.logger_id    = "App"        # or perhaps self.__class__.__name__
        logger = logging.getLogger( AppGlobal.logger_id )

        logger.handlers = []

        logger.setLevel( self.parameters.logging_level )

        # create the logging file handler
        fh = logging.FileHandler( self.parameters.pylogging_fn )

        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)

        # add handler to logger object -- want only one add may be a problem

        logger.info( "Done config_logger" )
        #print( "configured logger", flush = True )
        self.logger  = logger   # for access in rest of class?
        print( f"logger level in App = {self.logger.level}" )

    # --------------------------------------------
    def prog_info( self,  ):
        """
        print, log, or whatever information on the program
        """
        fll         = AppGlobal.force_log_level
        logger      = self.logger
        logger.log( fll, "" )
        logger.log( fll, "============================" )
        logger.log( fll, "" )
        title       =   "Application: "  + self.app_name + " " + self.app_version
        logger.log( 99, title )
        logger.log( 99, "" )

        if len( sys.argv ) == 0:
            logger.info( "no command line arg " )
        else:
            for ix_arg, i_arg in enumerate( sys.argv ):   # enumerate probably the right way
                msg = f"command line arg + {str( ix_arg ) }  =  { i_arg })"
                logger.log( AppGlobal.force_log_level, msg )

        logger.log( 99, f"current directory {os.getcwd()}"  )

        start_ts     = time.time()
        dt_obj       = datetime.datetime.utcfromtimestamp( start_ts )
        string_rep   = dt_obj.strftime('%Y-%m-%d %H:%M:%S')
        logger.log( 99, "Time now: " + string_rep )
        # logger_level( "Parameters say log to: " + self.parameters.pylogging_fn ) parameters and controller not available can get from logger_level

        return

    #-------------------------------------------
    def clean_up( self, ):
        """
        we are done, kill thread by sending signal
        make sure will break out of pause
        this should use app_state.set_state( )  !!
        self.worker_thread_run = False
        """
        msg = "join worker thread....."
        print( msg )
        self.logger.debug( msg )
        if self.worker_thread != None:
            self.worker_thread.join()
        msg = "end join"
        print( msg )
        self.logger.debug( msg )

        #self.backup_thread.close_backup(  )   # this is the problem
        #self.close_logs()

        msg = "cleanup, all done"
        print( msg, flush = True)
        self.logger.debug( msg )

    #-------------------------------------------
    def run_gui( self, ):
        """
        not right out of init because
        after create of app we are leaving time
        to set values
        """
        #print "start main loop please uncomment this was not the problem"

        AppGlobal.parameters.init_2()    # set the unset
        # self.app_state.info()

        try:
             self.gui.run()

        except Exception as err:
            self.logger.critical( "-------final run_gui----------" )
            self.logger.critical(  err,  stack_info=True )   # just where I am full trace back most info
            print( "exception in gui.run() see pylog", flush = True )
            raise
        #print "past main loop"
        # self.check_parms( None )   # None stands in for event not used
        # why not clean up here ??

    # ------------------------------------------
    def start_object_thread( self,  ):
        """
        call from gui thread

        """
        self.logger.info( "try to start_object_thread" )
        ok = self.app_state.set_state( "run" )
        if not( ok ):
            #print "change state to run not ok"
            self.logger.info( "change state to run not ok" )
            return

        self.gui.write_text( "Begin Backup start_object_thread\n" )

        self.worker_thread      = threading.Thread( target=self.backup_thread.run_backup , args=(  ) )
        # self.worker_thread_run  = True  !! fix in app_state
        self.worker_thread.start()

    # ----------------------------------
    def polling_begin( self,  ):
        """
        do this instead of test for init_fun set to something
        just about or ready to go link this in
        """
        print( "polling_begin", flush = True )
        self.init_fun()
        self.polling( )

    # ----------------------------------
    def polling( self,  ):
        """
        poll for messages from other threads
        queue has tuples ( where, msg )   where = "s" "d" or "b"   msg is message to post
        loop to empty the queue
        return noting but set up for next loop
        """
#        print( "directory_backup.py polling()" )
#        if self.init_fun != None:   # initialization function run this guy once  -- polling begin means this not needed
#            print( "polling_begin call init_fun()", flush = True )
#            self.init_fun()
#            self.init_fun            = None

        while True:
            try:
                data       = self.app_state.queue_to_tk.get_nowait()
                where, msg = data
            except queue.Empty:
                where   = ""

            if   where == "":
                # at top to save tests
                #self.gui.write_summary( msg + "\n" )
                pass
                break
            #* got some may be missing
            elif where == ">text":   #*
                self.gui.write_text( msg + "\n" )

            elif where == ">count":
                self.gui.display_app_state_count( )

            elif where == ">display_bytes":  # *
                self.gui.display_bytes( )

            elif where == ">display_dirs":  # *
                self.gui.display_dirs( )

            elif where == ">display_files": #*
                self.gui.display_files( )

            elif where == ">count_dir":
                self.gui.display_count_dir( )

            elif where == ">status": #*
                self.gui.display_app_state_status( )

            elif where == ">error":
                self.gui.display_errors(  )

#            elif where == "call":
#                pass
#                !! add a way to call a function  -- others might be implemented this way as well, add arguments?

            else:
                # #wfq  where from queue not understood
                log_msg = f"where from queue not understood>,  + {where} >> {msg}"
#                self.gui.write_log_text( log_msg + "\n" )
#                self.gui.write_log_text( msg     + "\n" )
                self.logger.critical(  log_msg )   #

        # this is wrong because may not have started yet ?? !!
        if   self.worker_thread != None :
            if not ( self.worker_thread.isAlive() ):
                # but queue may not yet be empty or what this come out early
                ps   = "Polling says Thread has stopped.\n"
                self.gui.write_text(  ps )
                #self.gui.write_summary( ps )
                self.worker_thread = None

        self.gui.root.after( self.polling_delta, self.polling )  # reschedule event

   # ----------------------------------
    def check_parms( self, ):

            # w.cget( "state" )
            print( "check_parms", flush = True )
            if self.backup_thread.validate_backup():
                self.app_state.set_state( "start_ok" )
            else:
                self.app_state.set_state( "start_ng" )

            self.gui.display_app_state_parms() #  build into valicate_backu not so easy as early return
#            if self.backup_thread.go_flag   == 1:

    # ------------------------------------------
    def set_backup_name( self, name ):
        """
        set a somewhat meaningful name for the backup
        """
        self.app_state.backup_name     = name

    # ------------------------------------------
    def set_source(self, a_dir, a_id = "ignore" ):
        """
        set the source directory and media id
        """
        self.app_state.sourceDir       = a_dir
        self.app_state.sourceMediaId   = a_id

    # ------------------------------------------
    def set_dest(self, a_dir, a_id = "ignore" ):
        self.app_state.destDir        = a_dir
        self.app_state.destMediaId    = a_id

    # --------------------------------------------
    def set_logs(self, detail_name, summary_name ):
        """
        for use by the external set up file
        ?? make one of the names auto off the first
        """
        self.app_state.log_detail_fn    = detail_name
        self.app_state.log_summary_fn   = summary_name

    # ------------------------------------------
    def add_filter_object(self, filter_object ):

        self.app_state.filter_objects.append( filter_object )

#    # ------------------------------------------
#    def set_max_dir_depth(self, max_dir_depth ):
#
#        self.app_state.max_dir_depth       = max_dir_depth

    # ------------------------------------------
    def set_simulate_mode(self, mode ):
        """

        """
        print(  "resolve why called "  )
#        self.app_state.simulate_mode_flag = mode

    # ------------------------------------------
    def set_log_skipped_flag( self, flag ):
        """

        """
        self.app_state.log_skipped_flag = flag

    # --------------------------------------------------
    def get_filter_text( self ):

        filter_texts   = []
        filter_objects = self.app_state.filter_objects

        for i_filter_object in filter_objects:
             filter_texts.append( str( i_filter_object ) )
        text  = "\n".join( filter_texts )

        return text

    # =============== button call back related
    # ------------------------------------------
    def start_object_thread_bcb( self,  ): # no event sent on call back function
        """
        call from gui thread, this is a button call back, just glue
        this is to start the backup
        indirectness is a bit odd
        """
        self.start_object_thread( )

   # ----------------------------------
    def pause_bcb( self,):
        """
        button call back, pause the app or run if already in pause
        """
        if (self.app_state.state == "pause" ):
            self.app_state.set_state( "run" ) # validity checked in set_state
        else:
            self.app_state.set_state( "pause" )

    # --------------------------------------------------
    def os_open_summary( self ):
        """
        used as callback from gui button
        """
        AppGlobal.os_open_txt_file( self.app_state.log_summary_fn )

    # --------------------------------------------------
    def os_open_detail( self ):
        """
        used as callback from gui button
        """
        AppGlobal.os_open_txt_file( self.app_state.log_detail_fn )

    # ----------------------------------------------
    def os_open_pylog( self,  ):
        """
        used as callback from gui button
        """
        AppGlobal.os_open_txt_file( self.parameters.pylogging_fn )

    # ----------------------------------------------
    def os_open_parmfile( self,  ):
        """
        used as callback from gui button
        """
        a_filename = self.parameters.application_dir  + os.path.sep + "parameters.py"   # and idea but not implemented

        AppGlobal.os_open_txt_file( a_filename )

    # ----------------------------------------------
    def os_open_helpfile( self,  ):
        """
        used as callback from gui button
        """
        AppGlobal.os_open_help_file( self.parameters.help_file )

# -----------  file filter objects  FF_ -------------

#  !! rename all to make sure described as filters   filter_for    xxx_ok_to_copy   .... ??
# !!make so filters can filter on file size and date

#Filter objects are created and setup in the gui thread ( or even before ) then the method
#check_file is run in the ( called ) from backup thread
#check_file returns True if the copy should go forward


# ================= class AllFiles =========================
class FFAll( ):
    """
    this is a filter object which accepts all file names
    !! what if the file is a directory, they should not be see check_file
    """
    def __init__( self,  ):
        self.filter_name      = "FFAll"         # class name
        self.filter_function  = "accept all files"   # description of the function
        pass

    #-------------------------------------------
    def check_file( self, file_name, src_dir, dest_dir  ):
        """
        this is the call that actually 'does' the filtering
        for arguments look were called.
        """
        return True

    #-------------------------------------------
    def __str__(self):
        return self.filter_name   + " function = " + self.filter_function



# ================= class AllFiles =========================
class FFExtList(  ):
    """
    this is a filter object which accepts all file names on a list of extensions
    no dot in extension   sample:  filter_object  = directory_backup.FFExtList( ["bat", "py"]  )
    we lower case the extension use this when you create your list
    """
    def __init__( self, list_of_extensions ):
        self.filter_name          = "FFExtList"         # class name
        self.filter_function      = "accept based on list of extensions "   # description of the function
        self.list_of_extensions   =  list_of_extensions

    #-------------------------------------------
    def check_file( self, file_name, src_dir, dest_dir  ):
        """
        this is the call that actually 'does' the filtering
        for arguments -- look were called.
        """
        splits     = file_name.split( "." )
        ext        =  splits[-1].lower()   # end one, could be several, fix case
        ok = False
#        for i_ext in self.list_of_extensions:
#            if
        if ext in self.list_of_extensions:
            ok = True


        return ok

    #-------------------------------------------
    def __str__(self):
        return self.filter_name   + " function = " + self.filter_function

# ================= class AllFiles =========================
#class AllFiles( FF_AllFiles  ):
#        pass    # deprecated, for compatibly




# ================= class AllFiles =========================
class FFPythonSource( ):
    """
    this is a filter object which accepts files for python ( and closely associated )
    change to use FFExtList
    """
    def __init__( self,  ):
        self.filter_name      = "FFPythonSource"
        self.filter_function  = "accept PythonSourceFiles  *.py, txt, bat, not pyc, not .zip or anything else"
        pass

    #-------------------------------------------
    def check_file( self, file_name, src_dir, dest_dir  ):

        # use a set test for in
        splits = file_name.split( "." )
        ext    =  splits[-1].lower()   # end one, could be several, fix case
        # !! use set and in
        if  ext == "py":
            return True
        elif ext == "txt":
            return True
        elif ext == "ico":
            return True
        elif ext == "bat":
            return True
        else:
            return False
        return False

    #-------------------------------------------
    def __str__(self):
        return self.filter_name   + " function = " + self.filter_function

# =============== utility   =================

# ================= class =========================
class FFMp4( ):
    """
    this is a filter object which accepts mp4 file names
    """
    def __init__( self,  ):
        self.filter_name      = "FFMp4"
        self.filter_function  = "accept *.mp4 files only"
        #pass
    #-------------------------------------------
    def check_file( self, file_name, src_dir, dest_dir  ):
        """
        this function checks a file name, in this one
        src_dir and dest_dir not used
        """
        splits = file_name.split( "." )
        ext    =  splits[-1].lower()   # end one, could be several, fix case
        #print "extension is ", ext
        if  ext == "mp4":
            return True
        else:
            return False

    #-------------------------------------------
    def __str__(self):
        return self.filter_name   + " function = " + self.filter_function

# ================= class =========================
class OneExtension( ):
    """
    this is a filter object which accepts files with a single extension
    extension is not case sensitive
    for windows may be ok on others
    """
    def __init__( self, ext ):
        self.filter_name      = "One Extension"
        self.filter_function  = "error extension not set"
        self.extension        = None
        self.set_extension( ext )

    #-------------------------------------------
    def set_extension( self, ext ):
        self.extension  = ext.lower()
        self.filter_function  = "accept *." + self.extension + " files only"

    #-------------------------------------------
    def check_file( self, file_name, src_dir, dest_dir  ):

        splits = file_name.split( "." )

        ext   =  splits[-1].lower()    # last part after the dot .
        if  ext == self.extension:
            return True
        else:
            return False

    #-------------------------------------------
    def __str__(self):
        if self.extension is None:
            self.filter_function  = "error extension not set"

        ret  = self.filter_name   + " function = " + self.filter_function
        return ret

# ================= class =========================
class FFADate( ):
    """
    !! looks like not done
    this is a filter object which accepts files with a single extension
    extension is not case sensitive
    for windows may be ok on others
    """
    def __init__( self, date_string ):
        self.filter_name      = "FFADate"
        self.filter_function  = "error extension not set"
        self.timestamp        = None
        self.date_string      = ""
        self.relation         = operator.lt    # default can be set file < adate        #  <, < = !=    import operator f_op    = operator.lt
        self.set_date( date_string )

    #-------------------------------------------
    def set_date( self, date_string ):

        a_format              = "%d/%m/%Y"
        self.timestamp        = time.mktime( datetime.datetime.strptime( date_string, a_format ).timetuple() )
        self.date_string      = date_string
        self.set_filter_function()

    #-------------------------------------------
    def set_relation( self, a_relation ):
        # looks like wip not working
        self.relation         =  a_relation
        self.set_filter_function()

    #-------------------------------------------
    def set_filter_function( self, ):
        # looks like wip
        self.filter_function  = "file date " + " relation " + str( self.relation ) + self.date_string + " files only"

    #-------------------------------------------
#    def check_file( self, file_name, src_dir, dest_dir  ):
#        return True

#    #-------------------------------------------
    def check_file( self, file_name, src_dir, dest_dir  ):
        """
        """
        # import os
        stats = os.stat( file_name )
        #print "stats are",file1,stats[8]
        modified = stats[8]
        print("in check file")
        a_check    = self.relation( modified, self.timestamp )
        #return True
        print(( "Adate check " + file_name + " " + str( a_check ) ))
        sys.stdout.flush()
        #return ( self.relation( modified, self.timestamp ) )
        return ( a_check )

    #-------------------------------------------
    def __str__(self):

        if self.timestamp == None:
            self.filter_function  = "error extension not set"

        ret  = self.filter_name   + " function = " + self.filter_function
        return ret

# -----------  End Filters   -------------

# =============== utillity   =================
def print_list( a_list ):
    for a_item in a_list:
        print( a_item )

#-------------------------------------------
def print_dict( a_dict ):
    a_list  = list( a_dict.items() )
    for a_item in a_list:
        print( a_item )


# -----------  test cases almost surely not be valid, check    -------------
# ====================== test cases =========================
#set_source(self, dir, id ):

def test_case_1( app ):
    """
    this was on a test directory
    """
    app.set_backup_name( "test_case_1 test, I:"   )

    app.set_source( r"D:\Temp\DeerAnti",                 "MainPart on 2TorNot2T"         )
    app.set_dest(   r"I:\Test\DeerAnti",                 "Buffalo 3T Disk"               )

    app.set_logs(   r"D:\logs\testDetail.log",          r"D:\logs\testSummary.log",     )   # detail, summary

    app.set_simulate_mode(     True )   # True to simulate, False really copy

    filter_object  = AllFiles( )     # need to default this and test it
    print(str( filter_object ))
    app.set_filter_object(   filter_object )

    #print("begin gui")
    app.run_gui()
    app.clean_up()


# ---------------------------------------------------------
def test_case_2( app ):
    """
    this was on a test directory
    """
    app.set_backup_name( "test_case_2 backup test, arduino local to D:"   )

    app.set_source( r"D:\Temp\DeerAnti",                 "MainPart on 2TorNot2T"         )
    app.set_source( r"D:\Russ\0000\Arduino",             "MainPart on 2TorNot2T"         )

    app.set_dest(   r"I:\Test\DeerAnti",                 "Buffalo 3T Disk"               )
    app.set_dest(   r"d:\Temp\Test\Arduino",             "MainPart on 2TorNot2T"         )

    app.set_logs(   r"D:\logs\testDetail.log",          r"D:\logs\testSummary.log",      )   # detail, summary

    app.set_simulate_mode(     True )   # True to simulate, False really copy

    filter_object  = AllFiles( )     # need to default this and test it
    print(str( filter_object ))
    app.set_filter_object(   filter_object )

    app.run_gui()

    app.clean_up()   # cant this be moved inside

# ------------------------------------------------------------
def backup_spyder( app ):

    app.set_backup_name( "Backup Spyder to I:"   )

    app.set_source( r"D:\Temp\DeerAnti",                 "MainPart on 2TorNot2T"         )
    app.set_source( r"D:\Russ\0000\Arduino",             "MainPart on 2TorNot2T"         )
    app.set_source( r"D:\Russ\0000\SpyderP",              "MainPart on 2TorNot2T"         )

    app.set_dest(   r"I:\Test\DeerAnti",                 "Buffalo 3T Disk"               )
    app.set_dest(   r"d:\Temp\Test\Arduino",             "MainPart on 2TorNot2T"         )
    app.set_dest(   r"I:\_Data\Russ\0000\SpyderP",       "Buffalo 3T Disk"               )

    app.set_logs(   r"D:\logs\testDetail.log",          r"D:\logs\testSummary.log",      )   # detail, summary

    app.set_simulate_mode(     False )   # True to simulate, False really copy

    filter_object  = AllFiles( )     # need to default this and test it
    print(str( filter_object ))
    app.set_filter_object(   filter_object )

    app.run_gui()

    app.clean_up()   # cant this be moved inside

# ------------------------------------------------------------
def test_backup_python2( ):

    app =  App(  )

    app.set_backup_name( r"Backup Test test_backup_python2( )"   )

    app.set_source(      r"D:\Russ\0000\python00\python2", "ignore" )

    app.set_dest(        r"I:\_Data\Russ\0000\python00\python2", "ignore" )

    app.set_logs(        r"D:\Russ\0000\python00\python3\_projects\backup_wip\BackupDetail.log",   r"D:\Russ\0000\python00\python3\_projects\backup_wip\BackupSummary.log",      )   # detail, summary

    app.set_simulate_mode( True )   # True to simulate, False really copy

    filter_object  = AllFiles( )     # need to default this and test it
    print( str( filter_object ) )
    app.add_filter_object(   filter_object )

#    filter_object  = FF_PythonSourceFiles( )     # need to default this and test it
#    print( str( filter_object ) )
#    app.add_filter_object(   filter_object )

    app.run_gui()

    app.clean_up()   # cant this be moved inside

# ------------------------------------------------------------
def test_backup_python3( ):

    app =  App(  )

    app.set_backup_name( r"Backup Test test_backup_python3( )"   )

    app.set_source(      r"D:\Russ\0000\python00\python3", "ignore" )

    app.set_dest(        r"I:\_Data\Russ\0000\python00\python3", "ignore" )

    app.set_logs(        r"D:\Russ\0000\python00\python3\_projects\backup_wip\BackupDetail3.log",   r"D:\Russ\0000\python00\python3\_projects\backup_wip\BackupSummary3.log",      )   # detail, summary

    app.set_simulate_mode( True )   # True to simulate, False really copy

    app.set_log_skipped_flag( False )

    filter_object  = AllFiles( )     # need to default this and test it
    print( str( filter_object ) )
    app.add_filter_object(   filter_object )

#    filter_object  = FF_PythonSourceFiles( )     # need to default this and test it
#    print( str( filter_object ) )
#    app.add_filter_object(   filter_object )

    app.run_gui()

    app.clean_up()   # cant this be moved inside


# ------------------------------------------------------------
def test_backup_TestSource( ):

    app =  App(  )

    app.set_backup_name( r"Backup Test test_backup_TestSource( )"   )

    app.set_source(      r"D:\Russ\0000\python00\python3\_projects\backup_wip\TestSource\Sub0", "ignore" )

    app.set_dest(        r"D:\Russ\0000\python00\python3\_projects\backup_wip\TestDest\Sub0", "ignore" )

    app.set_logs(        r"D:\Russ\0000\python00\python3\_projects\backup_wip\TestSource\BackupDetail.log",   r"D:\Russ\0000\python00\python3\_projects\backup_wip\TestSource\BackupSummary.log",      )   # detail, summary

    app.set_simulate_mode( False )   # True to simulate, False really copy

    filter_object  = AllFiles( )     # need to default this and test it
    print( str( filter_object ) )
    app.add_filter_object(   filter_object )

#    filter_object  = FF_PythonSourceFiles( )     # need to default this and test it
#    print( str( filter_object ) )
#    app.add_filter_object(   filter_object )
    self.parameters.init_2()

    app.run_gui()

    app.clean_up()   # cant this be moved inside

# ------------------------------------------------------------
def test_adate(  ):
    """
    test the filter function, not a backup test
    """
    ret = True
#    a_file_name    = r"D:\Russ\0000\SpyderP\Template\ex__ex.py"
#    filter_object  = ADate( "18/08/2016" )
#    ret = filter_object.check_file( a_file_name, "src_dir", "dest_dir"  )

    #ret = filter_object.check_file( "a_file_name", "src_dir", "dest_dir"  )   # will throw an uncaught error

    return ret

# ----------------------------------------------------
if __name__ == "__main__":
    pass
#======================
#    app = App(  )
#    #test_case_1( app )
#    #test_case_2(      app )
#    backup_spyder(    app )
##    test_case_2( app )

    #app.mainloop()
#======================
    #import os
    #print(( test_adate(  ) ))

# --------- tests for the wip --------------------------
    #test_backup_TestSource( )
    #test_backup_python2()
    test_backup_python3()

# ======================= eof =======================






